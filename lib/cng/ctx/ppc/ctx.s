#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2025 Dmitry Selyutin
# All rights reserved.
#

.section .text


.global cng_ctx_wrap
.type cng_ctx_wrap, STT_FUNC
cng_ctx_wrap:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    bl .Lcng_ctx_get
    lis %r7, .Ltrampoline@ha
    la %r7, .Ltrampoline@l(%r7)
    mtlr %r8
    b cng_ctx_ppc
.Ltrampoline:
    mr %r0, %r3
    mr %r3, %r4
    mr %r4, %r0
    mflr %r0
    stw %r0, 0x04(%r1)
    stw %r10, 0x14(%r1)
    lwz %r0, 0x18(%r1)
    mtctr %r0
    bctrl
    mr %r4, %r3
    lwz %r3, 0x14(%r1)
    b .Lcng_ctx_set


.global cng_ctx_get
.type cng_ctx_get, STT_FUNC
cng_ctx_get:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    li %r3, 0

.Lcng_ctx_get:
    stw %r8, 0x00(%r10)
    stw %r1, 0x04(%r10)
    stfd %f0, 0x08(%r10)
    stw %r9, 0x10(%r10)

    stw %r14, 0x14(%r10)
    stw %r15, 0x18(%r10)
    stw %r16, 0x1c(%r10)
    stw %r17, 0x20(%r10)
    stw %r18, 0x24(%r10)
    stw %r19, 0x28(%r10)
    stw %r20, 0x2c(%r10)
    stw %r21, 0x30(%r10)
    stw %r22, 0x34(%r10)
    stw %r23, 0x38(%r10)
    stw %r24, 0x3c(%r10)
    stw %r25, 0x40(%r10)
    stw %r26, 0x44(%r10)
    stw %r27, 0x48(%r10)
    stw %r28, 0x4c(%r10)
    stw %r29, 0x50(%r10)
    stw %r30, 0x54(%r10)
    stw %r31, 0x58(%r10)

    stfd %f14, 0x60(%r10)
    stfd %f15, 0x68(%r10)
    stfd %f16, 0x70(%r10)
    stfd %f17, 0x78(%r10)
    stfd %f18, 0x80(%r10)
    stfd %f19, 0x88(%r10)
    stfd %f20, 0x90(%r10)
    stfd %f21, 0x98(%r10)
    stfd %f22, 0xA0(%r10)
    stfd %f23, 0xA8(%r10)
    stfd %f24, 0xB0(%r10)
    stfd %f25, 0xB8(%r10)
    stfd %f26, 0xC0(%r10)
    stfd %f27, 0xC8(%r10)
    stfd %f28, 0xD0(%r10)
    stfd %f29, 0xD8(%r10)
    stfd %f30, 0xE0(%r10)
    stfd %f31, 0xE8(%r10)

    blr


.global cng_ctx_set
.type cng_ctx_set, STT_FUNC
cng_ctx_set:
.Lcng_ctx_set:
    mr %r0, %r3
    mr %r3, %r4
    mr %r4, %r0

    lwz %r5, 0x00(%r4)
    lwz %r1, 0x04(%r4)
    lfd %f0, 0x08(%r4)
    lwz %r8, 0x10(%r4)

    lwz %r14, 0x14(%r4)
    lwz %r15, 0x18(%r4)
    lwz %r16, 0x1c(%r4)
    lwz %r17, 0x20(%r4)
    lwz %r18, 0x24(%r4)
    lwz %r19, 0x28(%r4)
    lwz %r20, 0x2c(%r4)
    lwz %r21, 0x30(%r4)
    lwz %r22, 0x34(%r4)
    lwz %r23, 0x38(%r4)
    lwz %r24, 0x3c(%r4)
    lwz %r25, 0x40(%r4)
    lwz %r26, 0x44(%r4)
    lwz %r27, 0x48(%r4)
    lwz %r28, 0x4c(%r4)
    lwz %r29, 0x50(%r4)
    lwz %r30, 0x54(%r4)
    lwz %r31, 0x58(%r4)

    lfd %f14, 0x60(%r4)
    lfd %f15, 0x68(%r4)
    lfd %f16, 0x70(%r4)
    lfd %f17, 0x78(%r4)
    lfd %f18, 0x80(%r4)
    lfd %f19, 0x88(%r4)
    lfd %f20, 0x90(%r4)
    lfd %f21, 0x98(%r4)
    lfd %f22, 0xA0(%r4)
    lfd %f23, 0xA8(%r4)
    lfd %f24, 0xB0(%r4)
    lfd %f25, 0xB8(%r4)
    lfd %f26, 0xC0(%r4)
    lfd %f27, 0xC8(%r4)
    lfd %f28, 0xD0(%r4)
    lfd %f29, 0xD8(%r4)
    lfd %f30, 0xE0(%r4)
    lfd %f31, 0xE8(%r4)

    mtctr %r5
    mtfsf 0xff, %f0
    mtcr %r8

    bctr


.global cng_ctx_swap
.type cng_ctx_swap, STT_FUNC
cng_ctx_swap:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    bl .Lcng_ctx_get

    mr %r10, %r3
    mr %r3, %r4
    mr %r4, %r5
    b .Lcng_ctx_set
