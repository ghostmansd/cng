/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_ref {
    size_t count;
};
#define CNG_REF_INITIALIZER \
    { \
        .count = 1, \
    }


static inline void
cng_ref_init(struct cng_ref *ref) {
    ref->count = 1;
}


void
cng_ref_acquire(struct cng_ref *ref);


bool
cng_ref_release(struct cng_ref *ref);


#ifdef __cplusplus
} /* extern "C" */
#endif
