/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/bytes.h>

#include <stddef.h>


size_t
cng_string_merge(char *string, size_t size,
        char const *lstring, char const *rstring) {
    size_t rv;
    size_t const lsize = (cng_string_size(lstring) - 1);
    size_t const rsize = (cng_string_size(rstring) - 1);

    rv = cng_bytes_merge(string, size, lstring, lsize, rstring, rsize);
    if (rv < size)
        string[rv] = '\0';

    return (rv + 1);
}
