/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_slist {
    struct cng_slist *iter;
};
#define CNG_SLIST_INITIALIZER(VAR) \
    { \
        .iter = &(VAR), \
    }


static inline void *
cng_slist_entry(struct cng_slist const *link, size_t offset) {
    return (link ? (void *)(void const *)((unsigned char const *)link - offset) : NULL);
}
#define cng_slist_entry(TYPE, FIELD, LINK) \
    ((TYPE *)cng_slist_entry((LINK), offsetof(TYPE, FIELD)))


static inline void
cng_slist_init(struct cng_slist *link) {
    link->iter = link;
}


static inline bool
cng_slist_empty(struct cng_slist const *link) {
    return (link->iter == link);
}


static inline void
cng_slist_push_front(struct cng_slist *root, struct cng_slist *link) {
    link->iter = root->iter;
    root->iter = link;
}


static inline struct cng_slist *
cng_slist_pop_front(struct cng_slist *root) {
    struct cng_slist *link = root->iter;

    root->iter = link->iter;
    cng_slist_init(link);

    return link;
}


static inline struct cng_slist *
cng_slist_front(struct cng_slist const *root) {
    return (struct cng_slist *)root->iter;
}


#define cng_slist_front_entry(TYPE, FIELD, LINK) \
    cng_slist_entry(TYPE, FIELD, cng_slist_front(LINK))


#define cng_slist_foreach(VAR, ROOT) \
    for ((VAR) = &(ROOT)->iter; \
         *(VAR) != (ROOT); \
         (VAR) = &(*(VAR))->iter)


#define cng_slist_foreach_safe(VAR, ROOT, TMP) \
    for ((VAR) = &(ROOT)->iter; \
         (*(VAR) != (ROOT)) && (((TMP) = &(*(VAR))->iter), 1); \
         (VAR) = (TMP))


#define cng_slist_foreach_entry(TYPE, FIELD, VAR, ROOT) \
    for ((VAR) = cng_slist_front_entry(TYPE, FIELD, ROOT); \
         (VAR) && &(VAR)->FIELD != (ROOT); \
         (VAR) = cng_slist_front_entry(TYPE, FIELD, &(VAR)->FIELD))


#define cng_slist_foreach_entry_safe(TYPE, FIELD, VAR, ROOT, TMP) \
    for ((VAR) = cng_slist_front_entry(TYPE, FIELD, ROOT); \
         (VAR) && \
            (&(VAR)->FIELD != (ROOT)) && \
            (((TMP) = cng_slist_front_entry(TYPE, FIELD, &(VAR)->FIELD)), 1); \
         (VAR) = (TMP))


#ifdef __cplusplus
} /* extern "C" */
#endif
