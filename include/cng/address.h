/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


static inline void *
cng_address_min(void const *lhs, void const *rhs) {
    return (void *)(((uintptr_t)lhs < (uintptr_t)rhs) ? lhs : rhs);
}


static inline void *
cng_address_max(void const *lhs, void const *rhs) {
    return (void *)(((uintptr_t)lhs > (uintptr_t)rhs) ? lhs : rhs);
}


static inline int
cng_address_cmp(void const *lhs, void const *rhs) {
    if ((uintptr_t)lhs < (uintptr_t)rhs) {
        return -1;
    } else if ((uintptr_t)lhs > (uintptr_t)rhs) {
        return +1;
    } else {
        return 0;
    }
}


static inline bool
cng_address_lt(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) < 0);
}


static inline bool
cng_address_le(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) <= 0);
}


static inline bool
cng_address_eq(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) == 0);
}


static inline bool
cng_address_ne(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) != 0);
}


static inline bool
cng_address_ge(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) >= 0);
}


static inline bool
cng_address_gt(void const *lhs, void const *rhs) {
    return (cng_address_cmp(lhs, rhs) > 0);
}


static inline bool
cng_address_aligned2(void const *address, size_t bytes) {
    return (((uintptr_t)address & (bytes - 1)) == 0);
}


static inline bool
cng_address_decrease(void const **address, size_t bytes) {
    bool ok;
    uintptr_t const origin = (uintptr_t)*address;
    uintptr_t const result = (origin - bytes);

    if ((ok = (origin >= result)))
        *address = (void const *)result;

    return ok;
}


static inline bool
cng_address_increase(void const **address, size_t bytes) {
    bool ok;
    uintptr_t const origin = (uintptr_t)*address;
    uintptr_t const result = (origin + bytes);

    if ((ok = (origin <= result)))
        *address = (void const *)result;

    return ok;
}


static inline bool
cng_address_rounddown2(void const **address, size_t bytes) {
    size_t const mask = ~(bytes - 1);

    *address = (void const *)((uintptr_t)*address & mask);

    return !0;
}


static inline bool
cng_address_roundup2(void const **address, size_t bytes) {
    bool ok;
    size_t const align = (bytes - 1);
    size_t const mask = ~align;

    if ((ok = cng_address_increase(address, align)))
        *address = (void const *)((uintptr_t)*address & mask);

    return ok;
}


static inline void *
cng_address_heap(void const *head, void const *tail,
        size_t size, size_t align) {
    void const *iter = head;

    if (!cng_address_le(iter, tail) ||
            !cng_address_roundup2(&iter, align))
        return NULL;

    head = iter;

    if (!cng_address_increase(&head, size) ||
            !cng_address_le(&head, tail))
        return NULL;

    return (void *)iter;
}


static inline void *
cng_address_stack(void const *head, void const *tail,
        size_t size, size_t align) {
    if (!cng_address_ge(tail, head) ||
            !cng_address_rounddown2(&tail, align) ||
            !cng_address_decrease(&tail, size) ||
            !cng_address_ge(tail, head))
        return NULL;

    return (void *)tail;
}


#ifdef __cplusplus
} /* extern "C" */
#endif
