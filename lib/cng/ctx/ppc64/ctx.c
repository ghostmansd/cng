/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ctx.h>

#include <cng/address.h>
#include <cng/bytes.h>
#include <cng/size.h>

#include <assert.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


extern bool
cng_ctx_ppc64(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void));


struct cng_ctx_ppc64_stack {
    /* 0x00 */ alignas(32) uint64_t chain;
    /* 0x08 */ alignas(8) uint64_t cr;
    /* 0x10 */ alignas(8) uint64_t lr;
    /* 0x18 */ alignas(8) uint64_t compiler;
    /* 0x20 */ alignas(8) uint64_t linker;
    /* 0x28 */ alignas(8) uint64_t toc;
    /* 0x30 */ alignas(8) uint64_t callee;
    /* 0x38 */ alignas(8) uint64_t arg;
    /* 0x40 */ alignas(32) uint64_t nil;
    /* 0x48 */ alignas(8) uint64_t caller;
    /* 0x50 */ alignas(8) uint64_t entry;
    /* 0x58 */ alignas(8) uint64_t pad;
};


static_assert(
    sizeof(struct cng_ctx_ppc64_stack) == (sizeof(uint64_t) * 12),
    "sizeof(struct cng_ctx_ppc64_stack)"
);
static_assert(
    alignof(struct cng_ctx_ppc64_stack) == 32,
    "alignof(struct cng_ctx_ppc64_stack)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, chain) == (sizeof(uint64_t) * 0),
    "offsetof(struct cng_ctx_ppc64_stack, chain)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, cr) == (sizeof(uint64_t) * 1),
    "offsetof(struct cng_ctx_ppc64_stack, cr)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, lr) == (sizeof(uint64_t) * 2),
    "offsetof(struct cng_ctx_ppc64_stack, lr)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, compiler) == (sizeof(uint64_t) * 3),
    "offsetof(struct cng_ctx_ppc64_stack, compiler)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, linker) == (sizeof(uint64_t) * 4),
    "offsetof(struct cng_ctx_ppc64_stack, linker)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, toc) == (sizeof(uint64_t) * 5),
    "offsetof(struct cng_ctx_ppc64_stack, toc)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, callee) == (sizeof(uint64_t) * 6),
    "offsetof(struct cng_ctx_ppc64_stack, callee)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, arg) == (sizeof(uint64_t) * 7),
    "offsetof(struct cng_ctx_ppc64_stack, arg)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, nil) == (sizeof(uint64_t) * 8),
    "offsetof(struct cng_ctx_ppc64_stack, nil)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, caller) == (sizeof(uint64_t) * 9),
    "offsetof(struct cng_ctx_ppc64_stack, caller)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, entry) == (sizeof(uint64_t) * 10),
    "offsetof(struct cng_ctx_ppc64_stack, entry)"
);
static_assert(
    offsetof(struct cng_ctx_ppc64_stack, pad) == (sizeof(uint64_t) * 11),
    "offsetof(struct cng_ctx_ppc64_stack, pad)"
);


static struct cng_ctx_ppc64_stack *
cng_ctx_ppc64_stack(void *data, size_t size) {
    void const *head = data;
    void const *tail = data;
    struct cng_ctx_ppc64_stack *stack;

    if (!cng_address_increase(&tail, size))
        return false;

    stack = cng_address_stack(head, tail,
        sizeof(struct cng_ctx_ppc64_stack),
        alignof(struct cng_ctx_ppc64_stack));
    if (stack)
        cng_bytes_zero(stack, sizeof(*stack));

    return stack;
}


static void
cng_ctx_ppc64_setup(struct cng_ctx *ctx,
        struct cng_ctx_ppc64_stack *stack,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_ppc64 *ppc64 = &ctx->ppc64;

    stack->entry = (uint64_t)(uintptr_t)entry;
    stack->chain = (uint64_t)(uintptr_t)&stack->nil;

    ppc64->lr = (uint64_t)(uintptr_t)trampoline;
    ppc64->r1 = (uint64_t)(uintptr_t)&stack->chain;
}


bool
cng_ctx_ppc64(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_ppc64_stack *stack;

    stack = cng_ctx_ppc64_stack(data, size);
    if (!stack)
        return false;

    cng_ctx_ppc64_setup(ctx, stack, entry, trampoline);

    return true;
}
