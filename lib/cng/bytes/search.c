/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>
#include <cng/size.h>

#include <limits.h>
#include <stddef.h>


static inline void *
cng_bytes_search_byte(unsigned char byte,
        unsigned char const *data, size_t size) {
    while (size) {
        if (*data == byte)
            return (void *)data;

        ++data;
        --size;
    }

    return NULL;
}


static inline void *
cng_bytes_search_shift_or(size_t const table[256],
        unsigned char const *data, size_t size, size_t psize) {
    size_t state = ~(size_t)1;
    size_t const mask = ((size_t)1 << psize);
    unsigned char const *iter = data;
    unsigned char const *const tail = (iter + size);

    while (iter < tail) {
        state |= table[*iter++];
        state <<= 1;
        if ((state & mask) == 0)
            return (void *)(iter - psize);
    }

    return NULL;
}


static inline void *
cng_bytes_search_bmh(size_t const table[256],
        unsigned char const *data, size_t size,
        unsigned char const *pdata, size_t psize) {
    unsigned char const pbyte = pdata[--psize];

    while (size >= psize) {
        unsigned char const byte = data[psize];
        size_t const shift = cng_size_min(table[byte], size);

        if ((byte == pbyte) && cng_bytes_match(data, pdata, psize))
            return (void *)data;

        data += shift;
        size -= shift;
    }

    return NULL;
}


void *
cng_bytes_search(struct cng_bytes_pattern const *pattern,
        void const *data, size_t size) {
    size_t const psize = pattern->size;
    unsigned char const *pdata = pattern->data;
    size_t const *table = pattern->table;

    if (size < psize)
        return NULL;

    if (psize == 0)
        return (void *)data;

    if (psize == 1)
        return cng_bytes_search_byte(*pdata, data, size);

    if (psize < (sizeof(size_t) * CHAR_BIT))
        return cng_bytes_search_shift_or(table, data, size, psize);

    return cng_bytes_search_bmh(table, data, size, pdata, psize);
}
