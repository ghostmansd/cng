/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/slice.h>

#include <stdint.h>


struct cng_format_map_@TYPE@ {
    char const *const base_tdata;
    size_t const base_tsize;
    char const *const full_tdata;
    size_t const full_tsize;
};


static struct cng_format_map_@TYPE@ const *
cng_format_map_@TYPE@(char const *tdata, size_t tsize) {
    static struct cng_format_map_@TYPE@ const *entry;
    static struct cng_format_map_@TYPE@ const table[] = {
        {
            "2",
            (sizeof("2") - 1),
            "@TYPE@:2",
            (sizeof("@TYPE@:2") - 1),
        },
        {
            "4",
            (sizeof("4") - 1),
            "@TYPE@:4",
            (sizeof("@TYPE@:4") - 1),
        },
        {
            "8",
            (sizeof("8") - 1),
            "@TYPE@:8",
            (sizeof("@TYPE@:8") - 1),
        },
        {
            "16",
            (sizeof("16") - 1),
            "@TYPE@:16",
            (sizeof("@TYPE@:16") - 1),
        },
        {
            "32",
            (sizeof("32") - 1),
            "@TYPE@:32",
            (sizeof("@TYPE@:32") - 1),
        },
    };

    cng_array_foreach(entry, table) {
        if ((tsize == entry->base_tsize) &&
                cng_bytes_match(tdata, entry->base_tdata, tsize))
            return entry;
    }

    return NULL;
}


static size_t
cng_format_dump_@TYPE@(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    struct cng_format_map_@TYPE@ const *entry;

    if ((entry = cng_format_map_@TYPE@(tdata, tsize)) != NULL)
        return cng_format_vdump(&cng_format_stdint,
            data, size, entry->full_tdata, entry->full_tsize, ap);

    return 0;
}


static size_t
cng_format_load_@TYPE@(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    struct cng_format_map_@TYPE@ const *entry;

    if ((entry = cng_format_map_@TYPE@(tdata, tsize)) != NULL)
        return cng_format_vload(&cng_format_stdint,
            data, size, entry->full_tdata, entry->full_tsize, ap);

    return 0;
}


struct cng_format const cng_format_@TYPE@ = {
    cng_format_dump_@TYPE@,
    cng_format_load_@TYPE@,
};
