#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2025 Dmitry Selyutin
# All rights reserved.
#

.section .text


.global cng_ctx_wrap
.type cng_ctx_wrap, STT_FUNC
cng_ctx_wrap:
    pushl %ebp
    movl %esp, %ebp
    pushl %ebx
    calll .Lpc
    addl $_GLOBAL_OFFSET_TABLE_, %ebx
    leal .Ltrampoline@GOTOFF(%ebx), %eax
    subl $0x04, %esp
    pushl %eax
    pushl 0x14(%ebp)
    pushl 0x10(%ebp)
    pushl 0x0C(%ebp)
    pushl 0x08(%ebp)
    calll cng_ctx_x86@PLT
    addl $0x18, %esp
    popl %ebx
    popl %ebp
    retl
.Ltrampoline:
    movl %ecx, 0x00(%esp)
    movl %eax, 0x04(%esp)
    movl %edx, 0x10(%esp)
    calll *0x14(%esp)
    movl 0x10(%esp), %ecx
    jmp .Lcng_ctx_set
.Lpc:
    movl (%esp), %ebx
    retl


.global cng_ctx_get
.type cng_ctx_get, STT_FUNC
cng_ctx_get:
    leal 0x04(%esp), %edx
    movl 0x04(%esp), %ecx
    movl 0x00(%esp), %eax
.Lcng_ctx_get:
    movl %ebp, 0x14(%ecx)
    movl %edi, 0x10(%ecx)
    movl %esi, 0x0C(%ecx)
    movl %ebx, 0x08(%ecx)
    movl %edx, 0x04(%ecx)
    movl %eax, 0x00(%ecx)
    xorl %eax, %eax
    retl


.global cng_ctx_set
.type cng_ctx_set, STT_FUNC
cng_ctx_set:
    movl 0x08(%esp), %eax
    movl 0x04(%esp), %ecx
.Lcng_ctx_set:
    movl 0x14(%ecx), %ebp
    movl 0x10(%ecx), %edi
    movl 0x0C(%ecx), %esi
    movl 0x08(%ecx), %ebx
    movl 0x04(%ecx), %esp
    jmpl *0x00(%ecx)


.global cng_ctx_swap
.type cng_ctx_swap, STT_FUNC
cng_ctx_swap:
    leal 0x04(%esp), %edx
    movl 0x04(%esp), %ecx
    movl 0x00(%esp), %eax
    calll .Lcng_ctx_get

    movl 0x08(%esp), %edx
    movl 0x0C(%esp), %eax
    xchgl %edx, %ecx
    jmp .Lcng_ctx_set
