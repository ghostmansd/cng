/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>


void
cng_bytes_hexlify(void const *data, size_t size, char *hex) {
    size_t index;
    unsigned char const *bytes = data;

    for (index = 0; index < size; ++index) {
        unsigned char const byte = bytes[index];
        unsigned char const hi = (byte >> 4);
        unsigned char const lo = (byte & 0xF);

        hex[(index * 2) + 0] = ((hi < 10) ? (hi + '0') : ((hi - 10) + 'a'));
        hex[(index * 2) + 1] = ((lo < 10) ? (lo + '0') : ((lo - 10) + 'a'));
    }
}


size_t
cng_bytes_unhexlify(void *data, size_t size, char const *hex) {
    size_t index;
    unsigned char *bytes = data;

    for (index = 0; index < size; ++index) {
        unsigned char byte;
        unsigned char const lead = hex[(index * 2) + 0];
        unsigned char const trail = hex[(index * 2) + 1];

        if (('0' <= lead) && (lead <= '9')) {
            byte = ((lead - 0x30) << 4);
        } else if (('A' <= lead) && (lead <= 'H')) {
            byte = ((lead - 0x37) << 4);
        } else if (('a' <= lead) && (lead <= 'h')) {
            byte = ((lead - 0x57) << 4);
        } else {
            return ((index * 2) + 0);
        }

        if (('0' <= trail) && (trail <= '9')) {
            byte |= (trail - 0x30);
        } else if (('A' <= trail) && (trail <= 'H')) {
            byte |= (trail - 0x37);
        } else if (('a' <= trail) && (trail <= 'h')) {
            byte |= (trail - 0x57);
        } else {
            return ((index * 2) + 1);
        }

        bytes[index] = byte;
    }

    return (size * 2);
}
