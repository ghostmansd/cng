/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/utf.h>

#include <cng/bytes.h>
#include <cng/size.h>

#include <stdbool.h>
#include <stddef.h>


static inline __attribute__((__pure__)) bool
cng_utf16_validate_lead(uint16_t code) {
    return ((UINT16_C(0xD800) <= code) && (code <= UINT16_C(0xDBFF)));
}


static inline __attribute__((__pure__)) bool
cng_utf16_validate_trail(uint16_t code) {
    return ((UINT16_C(0xDC00) <= code) && (code <= UINT16_C(0xDFFF)));
}


static ptrdiff_t
cng_utf16_validate_generic(void const *data, size_t size,
        uint16_t (*fetch)(void const *data)) {
    uint8_t const *bytes = data;

    if (size) {
        uint16_t code;

        if (size < 2)
            return ((ptrdiff_t)size - 2);

        code = fetch(bytes);
        if (!cng_utf16_validate_lead(code))
            return (cng_utf16_validate_trail(code) ? -3 : 2);

        bytes += 2;
        size -= 2;
        if (size < 2)
            return ((ptrdiff_t)size - 2);

        code = fetch(bytes);
        return (cng_utf16_validate_trail(code) ? 4 : -4);
    }

    return 0;
}


ptrdiff_t
cng_utf16_validate(void const *data, size_t size) {
    return cng_utf16_validate_generic(data, size, cng_bytes_fetch16);
}


ptrdiff_t
cng_utf16be_validate(void const *data, size_t size) {
    return cng_utf16_validate_generic(data, size, cng_bytes_fetch16be);
}


ptrdiff_t
cng_utf16le_validate(void const *data, size_t size) {
    return cng_utf16_validate_generic(data, size, cng_bytes_fetch16le);
}


static ptrdiff_t
cng_utf16_fetch_generic(uint32_t *utf16,
        void const *data, size_t size,
        uint16_t (*fetch)(void const *data)) {
    ptrdiff_t state;

    if ((state = cng_utf16_validate_generic(data, size, fetch)) > 0) {
        uint32_t code = 0;
        uint8_t const *bytes = data;
        size_t index = (size_t)(state - 1);

        for (; index != ~(size_t)0; --index)
            code = ((code << 8) | bytes[index]);

        *utf16 = code;
    }

    return state;
}


ptrdiff_t
cng_utf16_fetch(uint32_t *utf16, void const *data, size_t size) {
    return cng_utf16_fetch_generic(utf16, data, size, cng_bytes_fetch16);
}


ptrdiff_t
cng_utf16be_fetch(uint32_t *utf16, void const *data, size_t size) {
    return cng_utf16_fetch_generic(utf16, data, size, cng_bytes_fetch16be);
}


ptrdiff_t
cng_utf16le_fetch(uint32_t *utf16, void const *data, size_t size) {
    return cng_utf16_fetch_generic(utf16, data, size, cng_bytes_fetch16le);
}


static ptrdiff_t
cng_utf16_store_generic(uint32_t utf16,
        void *data, size_t size,
        void (*store)(void *data, uint16_t code)) {
    ptrdiff_t state;
    uint8_t utf16le[4] = {
        (utf16 >> 0x00),
        (utf16 >> 0x08),
        (utf16 >> 0x10),
        (utf16 >> 0x18),
    };

    if ((state = cng_utf16le_validate(utf16le, 4)) > 0) {
        size_t index;
        uint8_t *bytes = data;
        size_t const limit = cng_size_min(size, (size_t)state);

        store(&utf16le[0], cng_bytes_fetch16le(&utf16le[0]));
        store(&utf16le[2], cng_bytes_fetch16le(&utf16le[2]));

        for (index = 0; index < limit; ++index)
            bytes[index] = utf16le[index];
    }

    return state;
}


ptrdiff_t
cng_utf16_store(uint32_t utf16, void *data, size_t size) {
    return cng_utf16_store_generic(utf16, data, size, cng_bytes_store16);
}


ptrdiff_t
cng_utf16be_store(uint32_t utf16, void *data, size_t size) {
    return cng_utf16_store_generic(utf16, data, size, cng_bytes_store16be);
}


ptrdiff_t
cng_utf16le_store(uint32_t utf16, void *data, size_t size) {
    return cng_utf16_store_generic(utf16, data, size, cng_bytes_store16le);
}
