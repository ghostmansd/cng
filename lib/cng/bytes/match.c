/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <cng/address.h>
#include <cng/size.h>

#include <stddef.h>
#include <stdint.h>


size_t
cng_bytes_match(void const *ldata, void const *rdata, size_t size) {
    size_t rv = 0;
    unsigned char const *lbytes = ldata;
    unsigned char const *rbytes = rdata;

    while (size &&
            (*lbytes == *rbytes) &&
            (!cng_address_aligned2(lbytes, sizeof(uintptr_t)) ||
             !cng_address_aligned2(rbytes, sizeof(uintptr_t)))) {
        ++lbytes;
        ++rbytes;
        --size;
        ++rv;
    }

    while ((size >= sizeof(uintptr_t)) &&
            (*(uintptr_t const *)lbytes == *(uintptr_t const *)rbytes)) {
        lbytes += sizeof(uintptr_t);
        rbytes += sizeof(uintptr_t);
        size -= sizeof(uintptr_t);
        rv += sizeof(uintptr_t);
    }

    while (size && (*lbytes == *rbytes)) {
        ++lbytes;
        ++rbytes;
        --size;
        ++rv;
    }

    return !size;
}
