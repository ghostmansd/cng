/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/utf.h>

#include <cng/bytes.h>
#include <cng/size.h>

#include <stdbool.h>
#include <stddef.h>


static inline __attribute__((__pure__)) bool
cng_utf32_validate_code(uint32_t code) {
    return (code <= UINT32_C(0x10FFFF));
}


static ptrdiff_t
cng_utf32_validate_generic(void const *data, size_t size,
        uint32_t (*fetch)(void const *data)) {
    if (size) {
        uint32_t code;

        if (size < 4)
            return ((ptrdiff_t)size - 4);

        code = fetch(data);
        return (cng_utf32_validate_code(code) ? 4 : -4);
    }

    return 0;
}


ptrdiff_t
cng_utf32_validate(void const *data, size_t size) {
    return cng_utf32_validate_generic(data, size, cng_bytes_fetch32);
}


ptrdiff_t
cng_utf32be_validate(void const *data, size_t size) {
    return cng_utf32_validate_generic(data, size, cng_bytes_fetch32be);
}


ptrdiff_t
cng_utf32le_validate(void const *data, size_t size) {
    return cng_utf32_validate_generic(data, size, cng_bytes_fetch32le);
}


static ptrdiff_t
cng_utf32_fetch_generic(uint32_t *utf32,
        void const *data, size_t size,
        uint32_t (*fetch)(void const *data)) {
    ptrdiff_t state;

    if ((state = cng_utf32_validate_generic(data, size, fetch)) > 0) {
        uint32_t code = 0;
        uint8_t const *bytes = data;
        size_t index = (size_t)(state - 1);

        for (; index != ~(size_t)0; --index)
            code = ((code << 8) | bytes[index]);

        *utf32 = code;
    }

    return state;
}


ptrdiff_t
cng_utf32_fetch(uint32_t *utf32, void const *data, size_t size) {
    return cng_utf32_fetch_generic(utf32, data, size, cng_bytes_fetch32);
}


ptrdiff_t
cng_utf32be_fetch(uint32_t *utf32, void const *data, size_t size) {
    return cng_utf32_fetch_generic(utf32, data, size, cng_bytes_fetch32be);
}


ptrdiff_t
cng_utf32le_fetch(uint32_t *utf32, void const *data, size_t size) {
    return cng_utf32_fetch_generic(utf32, data, size, cng_bytes_fetch32le);
}


static ptrdiff_t
cng_utf32_store_generic(uint32_t utf32,
        void *data, size_t size,
        void (*store)(void *data, uint32_t code)) {
    ptrdiff_t state;
    uint8_t utf32le[4] = {
        (utf32 >> 0x00),
        (utf32 >> 0x08),
        (utf32 >> 0x10),
        (utf32 >> 0x18),
    };

    if ((state = cng_utf32le_validate(utf32le, 4)) > 0) {
        size_t index;
        uint8_t *bytes = data;
        size_t const limit = cng_size_min(size, (size_t)state);

        store(utf32le, cng_bytes_fetch32le(utf32le));

        for (index = 0; index < limit; ++index)
            bytes[index] = utf32le[index];
    }

    return state;
}


ptrdiff_t
cng_utf32_store(uint32_t utf32, void *data, size_t size) {
    return cng_utf32_store_generic(utf32, data, size, cng_bytes_store32);
}


ptrdiff_t
cng_utf32be_store(uint32_t utf32, void *data, size_t size) {
    return cng_utf32_store_generic(utf32, data, size, cng_bytes_store32be);
}


ptrdiff_t
cng_utf32le_store(uint32_t utf32, void *data, size_t size) {
    return cng_utf32_store_generic(utf32, data, size, cng_bytes_store32le);
}
