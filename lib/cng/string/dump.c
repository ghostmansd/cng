/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/format.h>

#include <stdarg.h>
#include <stddef.h>


size_t
cng_string_dump(char const *string, char const *type,
        struct cng_format const *format, ...) {
    size_t rv;
    va_list ap;
    size_t const size = (cng_string_size(string) - 1);
    size_t const tsize = (cng_string_size(type) - 1);

    va_start(ap, format);
    rv = cng_format_vdump(format, string, size, type, tsize, ap);
    va_end(ap);

    return (rv + 1);
}
