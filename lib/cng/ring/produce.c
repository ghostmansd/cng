/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ring.h>

#include <stdatomic.h>
#include <stddef.h>

#include "ring.h"


static inline size_t
cng_ring_space_ext(struct cng_ring const *ring, size_t *reader, size_t *writer) {
    *writer = atomic_load_explicit(cng_ring_writer(ring), memory_order_relaxed);
    *reader = atomic_load_explicit(cng_ring_reader(ring), memory_order_acquire);

    return (ring->capacity - (*writer - *reader));
}


size_t
cng_ring_space(struct cng_ring const *ring) {
    return cng_ring_space_ext(ring, &(size_t){0}, &(size_t){0});
}


size_t
cng_ring_produce(struct cng_ring *ring, void const *data, size_t size) {
    size_t reader;
    size_t writer;
    size_t const space = cng_ring_space_ext(ring, &reader, &writer);

    if (space) {
        size_t index = 0;
        unsigned char const *bytes = data;
        unsigned char *storage = ring->storage;
        size_t const mask = (ring->capacity - 1);

        size = ((size < space) ? size : space);
        while (index < size)
            storage[writer++ & mask] = bytes[index++];

        atomic_store_explicit(cng_ring_writer(ring), writer, memory_order_release);

        return size;
    }

    return 0;
}
