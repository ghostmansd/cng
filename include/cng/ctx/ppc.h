/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


struct cng_ctx_ppc {
    /* 0x00 */ uint32_t lr;
    /* 0x04 */ uint32_t r1;
    /* 0x08 */ uint64_t fpscr;
    /* 0x10 */ uint32_t cr;
    /* 0x14 */ uint32_t r14;
    /* 0x18 */ uint32_t r15;
    /* 0x1C */ uint32_t r16;
    /* 0x20 */ uint32_t r17;
    /* 0x24 */ uint32_t r18;
    /* 0x28 */ uint32_t r19;
    /* 0x2C */ uint32_t r20;
    /* 0x30 */ uint32_t r21;
    /* 0x34 */ uint32_t r22;
    /* 0x38 */ uint32_t r23;
    /* 0x3C */ uint32_t r24;
    /* 0x40 */ uint32_t r25;
    /* 0x44 */ uint32_t r26;
    /* 0x48 */ uint32_t r27;
    /* 0x4C */ uint32_t r28;
    /* 0x50 */ uint32_t r29;
    /* 0x54 */ uint32_t r30;
    /* 0x58 */ uint32_t r31;
    /* 0x5C */ uint32_t : 32;
    /* 0x60 */ uint64_t f14;
    /* 0x68 */ uint64_t f15;
    /* 0x70 */ uint64_t f16;
    /* 0x78 */ uint64_t f17;
    /* 0x80 */ uint64_t f18;
    /* 0x88 */ uint64_t f19;
    /* 0x90 */ uint64_t f20;
    /* 0x98 */ uint64_t f21;
    /* 0xA0 */ uint64_t f22;
    /* 0xA8 */ uint64_t f23;
    /* 0xB0 */ uint64_t f24;
    /* 0xB8 */ uint64_t f25;
    /* 0xC0 */ uint64_t f26;
    /* 0xC8 */ uint64_t f27;
    /* 0xD0 */ uint64_t f28;
    /* 0xD8 */ uint64_t f29;
    /* 0xE0 */ uint64_t f30;
    /* 0xE8 */ uint64_t f31;
};


#ifdef __cplusplus
} /* extern "C" */
#endif
