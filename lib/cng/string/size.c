/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/utf.h>

size_t
cng_string_size(char const *string) {
    unsigned char byte;
    unsigned char const *bytes = (unsigned char const *)string;

    do {
        byte = *bytes;
        bytes += (cng_utf8_utf32_order(byte) + 1);
    } while (byte);

    return (bytes - (unsigned char const *)string);
}
