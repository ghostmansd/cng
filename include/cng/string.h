/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_format;


size_t
cng_string_copy(char *target, size_t limit, char const *source);


size_t
cng_string_dump(char const *string, char const *type,
        struct cng_format const *format, ...);


size_t
cng_string_load(char *string, size_t size, char const *type,
        struct cng_format const *format, ...);


bool
cng_string_match(char const *lstring, char const *rstring);


bool
cng_string_match_prefix(char const *corpus, char const *pattern);


bool
cng_string_match_suffix(char const *corpus, char const *pattern);


size_t
cng_string_merge(char *string, size_t size,
        char const *lstring, char const *rstring);


char const *
cng_string_runes(char const *string, uint32_t *rune);


size_t
cng_string_size(char const *string);


#ifdef __cplusplus
} /* extern "C" */
#endif
