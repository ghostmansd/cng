/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdarg.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_format {
    size_t (*dump)(char const *data, size_t size,
            char const *tdata, size_t tsize,
            va_list ap);
    size_t (*load)(char *data, size_t size,
            char const *tdata, size_t tsize,
            va_list ap);
};


extern struct cng_format const cng_format_bool;
extern struct cng_format const cng_format_bytes;
extern struct cng_format const cng_format_stdint;
extern struct cng_format const cng_format_rune;
extern struct cng_format const cng_format_pointer;
extern struct cng_format const cng_format_int8;
extern struct cng_format const cng_format_uint8;
extern struct cng_format const cng_format_int16;
extern struct cng_format const cng_format_uint16;
extern struct cng_format const cng_format_int32;
extern struct cng_format const cng_format_uint32;
extern struct cng_format const cng_format_int64;
extern struct cng_format const cng_format_uint64;
extern struct cng_format const cng_format_schar;
extern struct cng_format const cng_format_uchar;
extern struct cng_format const cng_format_short;
extern struct cng_format const cng_format_ushort;
extern struct cng_format const cng_format_int;
extern struct cng_format const cng_format_uint;
extern struct cng_format const cng_format_long;
extern struct cng_format const cng_format_ulong;
extern struct cng_format const cng_format_llong;
extern struct cng_format const cng_format_ullong;


size_t
cng_format_dump(struct cng_format const *format,
        char const *data, size_t size,
        char const *tdata, size_t tsize,
        ...);


size_t
cng_format_vdump(struct cng_format const *format,
        char const *data, size_t size,
        char const *tdata, size_t tsize,
        va_list ap);


size_t
cng_format_load(struct cng_format const *format,
        char *data, size_t size,
        char const *tdata, size_t tsize,
        ...);


size_t
cng_format_vload(struct cng_format const *format,
        char *data, size_t size,
        char const *tdata, size_t tsize,
        va_list ap);


#ifdef __cplusplus
} /* extern "C" */
#endif
