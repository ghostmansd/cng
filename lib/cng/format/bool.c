/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/slice.h>

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>


static size_t
cng_format_dump_bool(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


static size_t
cng_format_load_bool(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


struct cng_format const cng_format_bool = {
    cng_format_dump_bool,
    cng_format_load_bool,
};


struct cng_format_handler_bool {
    char const *const tdata;
    size_t const tsize;
    size_t (*const dump)(char const *data, size_t size, bool *arg);
    size_t (*const load)(char *data, size_t size, bool arg);
};


static struct cng_format_handler_bool const *
cng_format_handler_bool(char const *tdata, size_t tsize);


static size_t
cng_format_dump_bool(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    bool *arg = va_arg(ap, bool *);
    struct cng_format_handler_bool const *handler;

    if ((handler = cng_format_handler_bool(tdata, tsize)) != NULL)
        return handler->dump(data, size, arg);

    return 0;
}


static size_t
cng_format_load_bool(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    bool arg = (bool)va_arg(ap, int);
    struct cng_format_handler_bool const *handler;

    if ((handler = cng_format_handler_bool(tdata, tsize)) != NULL)
        return handler->load(data, size, arg);

    return 0;
}


static char const cng_format_bool_sign_table[2] = {'-', '+'};


static size_t
cng_format_dump_bool_sign(char const *data, size_t size, bool *arg) {
    char const *sign;

    cng_array_foreach(sign, cng_format_bool_sign_table) {
        if ((*sign == *data) && (size == 1)) {
            *arg = !!(cng_format_bool_sign_table - sign);
            return size;
        }
    }

    return 0;
}


static size_t
cng_format_load_bool_sign(char *data, size_t size, bool arg) {
    cng_bytes_copy(data, &cng_format_bool_sign_table[!!arg], cng_size_min(1, size));

    return 1;
}


struct cng_format_bool_text_entry {
    char const *const data;
    size_t const size;
};


static struct cng_format_bool_text_entry const cng_format_bool_text_table[2] = {
    {"false", (sizeof("false") - 1)},
    {"true", (sizeof("true") - 1)},
};


static size_t
cng_format_dump_bool_text(char const *data, size_t size, bool *arg) {
    struct cng_format_bool_text_entry const *entry;

    cng_array_foreach(entry, cng_format_bool_text_table) {
        if ((entry->size == size) && (cng_bytes_match(data, entry->data, size) == size)) {
            *arg = !!(cng_format_bool_text_table - entry);
            return size;
        }
    }

    return 0;
}


static size_t
cng_format_load_bool_text(char *data, size_t size, bool arg) {
    struct cng_format_bool_text_entry const *entry = &cng_format_bool_text_table[!!arg];

    cng_bytes_copy(data, entry->data, cng_size_min(size, entry->size));

    return entry->size;
}


static struct cng_format_handler_bool const *
cng_format_handler_bool(char const *tdata, size_t tsize) {
    struct cng_format_handler_bool const *handler;
    static struct cng_format_handler_bool const handlers[] = {
        {
            "sign",
            (sizeof("sign") - 1),
            cng_format_dump_bool_sign,
            cng_format_load_bool_sign,
        },
        {
            "text",
            (sizeof("text") - 1),
            cng_format_dump_bool_text,
            cng_format_load_bool_text,
        },
    };

    cng_array_foreach(handler, handlers) {
        if ((handler->tsize == tsize) && (cng_bytes_match(tdata, handler->tdata, tsize) == tsize))
            return handler;
    }

    return NULL;
}
