/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ring.h>

#include <stdatomic.h>
#include <stddef.h>

#include "ring.h"


static inline size_t
cng_ring_count_ext(struct cng_ring const *ring, size_t *reader, size_t *writer) {
    *reader = atomic_load_explicit(cng_ring_reader(ring), memory_order_relaxed);
    *writer = atomic_load_explicit(cng_ring_writer(ring), memory_order_acquire);

    return (*writer - *reader);
}


size_t
cng_ring_count(struct cng_ring const *ring) {
    return cng_ring_count_ext(ring, &(size_t){0}, &(size_t){0});
}


size_t
cng_ring_consume(struct cng_ring *ring, void *data, size_t size) {
    size_t reader;
    size_t writer;
    size_t const count = cng_ring_count_ext(ring, &reader, &writer);

    if (count) {
        size_t index = 0;
        unsigned char *bytes = data;
        unsigned char const *storage = ring->storage;
        size_t const mask = (ring->capacity - 1);

        size = ((size < count) ? size : count);
        while (index < size)
            bytes[index++] = storage[reader++ & mask];

        atomic_store_explicit(cng_ring_reader(ring), reader, memory_order_release);

        return size;
    }

    return 0;
}
