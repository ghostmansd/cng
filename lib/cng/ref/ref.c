/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ref.h>

#include <stdalign.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>


_Static_assert(sizeof(size_t) == sizeof(atomic_size_t),
               "sizeof(size_t) == sizeof(atomic_size_t)");

_Static_assert(alignof(size_t) == alignof(atomic_size_t),
               "alignof(size_t) == alignof(atomic_size_t)");


void
cng_ref_acquire(struct cng_ref *ref) {
    (void)atomic_fetch_add_explicit((atomic_size_t *)&ref->count, 1, memory_order_relaxed);
}


bool
cng_ref_release(struct cng_ref *ref) {
    if (atomic_fetch_sub_explicit((atomic_size_t *)&ref->count, 1, memory_order_release) == 1) {
        atomic_thread_fence(memory_order_acquire);
        return true;
    }

    return false;
}
