/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ctx.h>

#include <cng/address.h>
#include <cng/bytes.h>
#include <cng/size.h>

#include <assert.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


extern bool
cng_ctx_x86_64(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void));


struct cng_ctx_x86_64_stack {
    /* 0x00 */ alignas(16) uint64_t pad0;
    /* 0x08 */ alignas(8) uint64_t code;
    /* 0x10 */ alignas(16) uint64_t nil;
    /* 0x18 */ alignas(8) uint64_t pad1;
    /* 0x20 */ alignas(16) uint64_t caller;
    /* 0x28 */ alignas(8) uint64_t entry;
};


static_assert(
    sizeof(struct cng_ctx_x86_64_stack) == (sizeof(uint64_t) * 6),
    "sizeof(struct cng_ctx_x86_64_stack)"
);
static_assert(
    alignof(struct cng_ctx_x86_64_stack) == 16,
    "alignof(struct cng_ctx_x86_64_stack)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, pad0) == (sizeof(uint64_t) * 0),
    "offsetof(struct cng_ctx_x86_64_stack, pad0)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, code) == (sizeof(uint64_t) * 1),
    "offsetof(struct cng_ctx_x86_64_stack, code)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, nil) == (sizeof(uint64_t) * 2),
    "offsetof(struct cng_ctx_x86_64_stack, nil)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, pad1) == (sizeof(uint64_t) * 3),
    "offsetof(struct cng_ctx_x86_64_stack, pad1)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, caller) == (sizeof(uint64_t) * 4),
    "offsetof(struct cng_ctx_x86_64_stack, caller)"
);
static_assert(
    offsetof(struct cng_ctx_x86_64_stack, entry) == (sizeof(uint64_t) * 5),
    "offsetof(struct cng_ctx_x86_64_stack, entry)"
);


static struct cng_ctx_x86_64_stack *
cng_ctx_x86_64_stack(void *data, size_t size) {
    void const *head = data;
    void const *tail = data;
    struct cng_ctx_x86_64_stack *stack;

    if (!cng_address_increase(&tail, size))
        return false;

    stack = cng_address_stack(head, tail,
        sizeof(struct cng_ctx_x86_64_stack),
        alignof(struct cng_ctx_x86_64_stack));
    if (stack)
        cng_bytes_zero(stack, sizeof(*stack));

    return stack;
}


static void
cng_ctx_x86_64_setup(struct cng_ctx *ctx,
        struct cng_ctx_x86_64_stack *stack,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_x86_64 *x86_64 = &ctx->x86_64;

    stack->entry = (uint64_t)(uintptr_t)entry;

    x86_64->rsp = (uint64_t)(uintptr_t)((uint8_t *)&stack->code + sizeof(stack->code));
    x86_64->rip = (uint64_t)(uintptr_t)trampoline;
    x86_64->rbx = (uint64_t)(uintptr_t)&stack->nil;
}


bool
cng_ctx_x86_64(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_x86_64_stack *stack;

    if ((stack = cng_ctx_x86_64_stack(data, size)) == NULL)
        return false;

    cng_ctx_x86_64_setup(ctx, stack, entry, trampoline);

    return true;
}
