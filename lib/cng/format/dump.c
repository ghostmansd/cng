/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <stdarg.h>
#include <stddef.h>


size_t
cng_format_vdump(struct cng_format const *format,
        char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    return (format ? format->dump(data, size, tdata, tsize, ap) : ~(size_t)0);
}


size_t
cng_format_dump(struct cng_format const *format,
        char const *data, size_t size,
        char const *tdata, size_t tsize, ...) {
    size_t rv;
    va_list ap;

    va_start(ap, tsize);
    rv = cng_format_vdump(format, data, size, tdata, tsize, ap);
    va_end(ap);

    return rv;
}
