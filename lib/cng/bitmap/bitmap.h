/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <cng/bitmap.h>

#include <stdalign.h>
#include <stdatomic.h>
#include <stddef.h>


_Static_assert(sizeof(size_t) == sizeof(atomic_size_t),
               "sizeof(size_t) == sizeof(atomic_size_t)");

_Static_assert(alignof(size_t) == alignof(atomic_size_t),
               "alignof(size_t) == alignof(atomic_size_t)");


static inline atomic_size_t *
cng_bitmap_bucket(size_t const *bitmap, size_t bit) {
    return (atomic_size_t *)&bitmap[bit / CNG_BITMAP_BITS];
}


static inline __attribute__((__pure__)) size_t
cng_bitmap_mask(size_t bit) {
    return ((size_t)1 << (bit % CNG_BITMAP_BITS));
}
