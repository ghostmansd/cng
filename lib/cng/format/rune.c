/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/slice.h>

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>


static size_t
cng_format_dump_rune(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


static size_t
cng_format_load_rune(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


struct cng_format const cng_format_rune = {
    cng_format_dump_rune,
    cng_format_load_rune,
};


struct cng_format_handler_rune {
    char const *const tdata;
    size_t const tsize;
    size_t (*const dump)(char const *data, size_t size, uint32_t *arg);
    size_t (*const load)(char *storage, uint32_t arg);
};


static struct cng_format_handler_rune const *
cng_format_handler_rune(char const *tdata, size_t tsize);


static size_t
cng_format_dump_rune(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    uint32_t *rune = va_arg(ap, uint32_t *);
    struct cng_format_handler_rune const *handler;

    if ((handler = cng_format_handler_rune(tdata, tsize)))
        return handler->dump(data, size, rune);

    return 0;
}


static size_t
cng_format_load_rune(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    uint32_t const rune = va_arg(ap, uint32_t);
    struct cng_format_handler_rune const *handler;

    if ((handler = cng_format_handler_rune(tdata, tsize))) {
        size_t rv;
        char storage[sizeof("\\U0010ffff")];

        rv = handler->load(storage, rune);
        cng_bytes_copy(data, storage, cng_size_min(rv, size));

        return rv;
    }

    return 0;
}


static size_t
cng_format_dump_rune_code(char const *data, size_t size, uint32_t *arg) {
    size_t rv;
    char type;
    size_t limit;
    uint32_t rune;
    unsigned char bytes[sizeof(uint32_t)];

    if (!size || (*data++ != '\\'))
        return 0;

    type = *data++;
    if ((type != 'u') && (type != 'U'))
        return 1;

    limit = ((type == 'u') ? 4 : 8);
    if (size != (2 + limit))
        return (2 + limit);

    if ((rv = cng_bytes_unhexlify(bytes, (limit / 2), data)) == limit) {
        if (type == 'u')
            rune = (uint32_t)cng_bytes_fetch16be(bytes);
        else
            rune = cng_bytes_fetch32be(bytes);

        *arg = rune;

        return size;
    }

    return rv;
}


static size_t
cng_format_load_rune_code(char *storage, uint32_t arg) {
    unsigned char bytes[sizeof(uint32_t)];

    storage[0] = '\\';
    if (arg < UINT32_C(0xFFFF)) {
        storage[1] = 'u';
        cng_bytes_store16be(bytes, (uint16_t)arg);
        cng_bytes_hexlify(bytes, sizeof(uint16_t), &storage[2]);
        return 6;
    } else {
        storage[1] = 'U';
        cng_bytes_store32be(bytes, arg);
        cng_bytes_hexlify(bytes, sizeof(uint32_t), &storage[2]);
        return 10;
    }
}


static struct cng_format_handler_rune const *
cng_format_handler_rune(char const *tdata, size_t tsize) {
    struct cng_format_handler_rune const *handler;
    static struct cng_format_handler_rune const handlers[] = {
        {
            "code",
            (sizeof("code") - 1),
            cng_format_dump_rune_code,
            cng_format_load_rune_code,
        },
    };

    cng_array_foreach(handler, handlers) {
        if ((handler->tsize == tsize) && (cng_bytes_match(tdata, handler->tdata, tsize) == tsize))
            return handler;
    }

    return NULL;
}
