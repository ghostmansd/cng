/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <stddef.h>


extern void
(*volatile cng_bytes_wipe_cb)(void *data, size_t size);

void
(*volatile cng_bytes_wipe_cb)(void *data, size_t size) = cng_bytes_zero;


void
cng_bytes_wipe(void *data, size_t size) {
    (*cng_bytes_wipe_cb)(data, size);
}
