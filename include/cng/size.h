/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


static inline size_t
cng_size_min(size_t lhs, size_t rhs) {
    return ((lhs < rhs) ? lhs : rhs);
}


static inline size_t
cng_size_max(size_t lhs, size_t rhs) {
    return ((lhs > rhs) ? lhs : rhs);
}


static inline bool
cng_size_aligned2(size_t size, size_t value) {
    return ((size & (value - 1)) == 0);
}


static inline bool
cng_size_decrease(size_t *size, size_t value) {
    bool ok;
    size_t const origin = *size;
    size_t const result = (origin - value);

    if ((ok = (origin >= result)))
        *size = result;

    return ok;
}


static inline bool
cng_size_increase(size_t *size, size_t value) {
    bool ok;
    size_t const origin = *size;
    size_t const result = (origin + value);

    if ((ok = (origin <= result)))
        *size = result;

    return ok;
}


static inline bool
cng_size_multiply(size_t *size, size_t value) {
    bool ok;
    size_t const origin = *size;
    size_t const result = (origin * value);

    if ((ok = ((origin == 0) || ((result / origin) == value))))
        *size = result;

    return ok;
}


static inline bool
cng_size_rounddown2(size_t *size, size_t value) {
    size_t const mask = ~(value - 1);

    *size = (*size & mask);

    return true;
}


static inline bool
cng_size_roundup2(size_t *size, size_t value) {
    bool ok;
    size_t const align = (value - 1);
    size_t const mask = ~align;

    if ((ok = cng_size_increase(size, value)))
        *size = (*size & mask);

    return ok;
}


#ifdef __cplusplus
} /* extern "C" */
#endif
