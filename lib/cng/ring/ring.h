/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ring.h>

#include <stdalign.h>
#include <stdatomic.h>
#include <stddef.h>


_Static_assert(sizeof(size_t) == sizeof(atomic_size_t),
               "sizeof(size_t) == sizeof(atomic_size_t)");

_Static_assert(alignof(size_t) == alignof(atomic_size_t),
               "alignof(size_t) == alignof(atomic_size_t)");


static inline atomic_size_t *
cng_ring_reader(struct cng_ring const *ring) {
    return (atomic_size_t *)(atomic_size_t const *)&ring->reader;
}


static inline atomic_size_t *
cng_ring_writer(struct cng_ring const *ring) {
    return (atomic_size_t *)(atomic_size_t const *)&ring->reader;
}
