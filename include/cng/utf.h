/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/* UTF-8 */
#define CNG_UTF8_BOM "\xEF\xBB\xBF"


__attribute__((__const__)) ptrdiff_t
cng_utf8_validate(void const *data, size_t size);


ptrdiff_t
cng_utf8_fetch(uint32_t *utf8, void const *data, size_t size);


ptrdiff_t
cng_utf8_store(uint32_t utf8, void *data, size_t size);


static inline __attribute__((__pure__)) uint32_t
cng_utf8_utf16(uint32_t utf8);


static inline __attribute__((__pure__)) uint32_t
cng_utf8_utf32(uint32_t utf8);


/* UTF-16 */
#define CNG_UTF16BE_BOM "\xFE\xFF"
#define CNG_UTF16LE_BOM "\xFF\xFE"


__attribute__((__const__)) ptrdiff_t
cng_utf16_validate(void const *data, size_t size);


__attribute__((__const__)) ptrdiff_t
cng_utf16be_validate(void const *data, size_t size);


__attribute__((__const__)) ptrdiff_t
cng_utf16le_validate(void const *data, size_t size);


ptrdiff_t
cng_utf16_fetch(uint32_t *utf16, void const *data, size_t size);


ptrdiff_t
cng_utf16be_fetch(uint32_t *utf16, void const *data, size_t size);


ptrdiff_t
cng_utf16le_fetch(uint32_t *utf16, void const *data, size_t size);


ptrdiff_t
cng_utf16_store(uint32_t utf16, void *data, size_t size);


ptrdiff_t
cng_utf16be_store(uint32_t utf16, void *data, size_t size);


ptrdiff_t
cng_utf16le_store(uint32_t utf16, void *data, size_t size);


static inline __attribute__((__pure__)) uint32_t
cng_utf16_utf8(uint32_t utf16);


static inline __attribute__((__pure__)) uint32_t
cng_utf16_utf32(uint32_t utf16);


/* UTF-32 */
#define CNG_UTF32BE_BOM "\x00\x00\xFE\xFF"
#define CNG_UTF32LE_BOM "\xFF\xFE\x00\x00"

__attribute__((__const__)) ptrdiff_t
cng_utf32_validate(void const *data, size_t size);


__attribute__((__const__)) ptrdiff_t
cng_utf32be_validate(void const *data, size_t size);


__attribute__((__const__)) ptrdiff_t
cng_utf32le_validate(void const *data, size_t size);


ptrdiff_t
cng_utf32_fetch(uint32_t *utf32, void const *data, size_t size);


ptrdiff_t
cng_utf32be_fetch(uint32_t *utf32, void const *data, size_t size);


ptrdiff_t
cng_utf32le_fetch(uint32_t *utf32, void const *data, size_t size);


ptrdiff_t
cng_utf32_store(uint32_t utf32, void *data, size_t size);


ptrdiff_t
cng_utf32be_store(uint32_t utf32, void *data, size_t size);


ptrdiff_t
cng_utf32le_store(uint32_t utf32, void *data, size_t size);


static inline __attribute__((__pure__)) uint32_t
cng_utf32_utf8(uint32_t utf32);


static inline __attribute__((__pure__)) uint32_t
cng_utf32_utf16(uint32_t utf32);


/* UTF-8 */
static inline __attribute__((__pure__)) uint32_t
cng_utf8_utf16(uint32_t utf8) {
    return cng_utf32_utf16(cng_utf8_utf32(utf8));
}


static inline __attribute__((__pure__)) uint32_t
cng_utf8_utf32_order(uint32_t utf8) {
    return ((UINT32_C(0xE5000000) >> (((utf8 & 0xF0U) >> 4U) << 1U)) & 3);
}


static inline __attribute__((__pure__)) uint32_t
cng_utf8_utf32(uint32_t utf8) {
    uint32_t utf32 = 0;
    uint32_t const order = cng_utf8_utf32_order(utf8);
    uint32_t const mask = ((UINT32_C(0x070F1F7F) >> (order << 3)) & 0xFF);
    uint32_t const shift = ((UINT32_C(0x00060C12) >> (order << 3)) & 0xFF);

    utf32 |= ((utf8 & mask) << 0x12);
    utf32 |= (((utf8 >> 0x08) & 0x3F) << 0x0C);
    utf32 |= (((utf8 >> 0x10) & 0x3F) << 0x06);
    utf32 |= (((utf8 >> 0x18) & 0x3F) << 0x00);
    utf32 >>= shift;

    return utf32;
}


/* UTF-16 */
static inline __attribute__((__pure__)) uint32_t
cng_utf16_utf8(uint32_t utf16) {
    return cng_utf32_utf8(cng_utf16_utf32(utf16));
}


static inline __attribute__((__pure__)) uint32_t
cng_utf16_utf32(uint32_t utf16) {
    uint32_t const trail = (utf16 >> 0x10);
    uint32_t const lead = (utf16 & UINT32_C(0xFFFF));

    if (trail) {
        uint16_t const lo =
            (((utf16 & UINT32_C(0xFFFF)) - UINT32_C(0xD800)) << 10);
        uint16_t const hi = ((utf16 >> 16) - UINT32_C(0xDC00));

        return ((uint32_t)lo + (uint32_t)hi + UINT32_C(0x10000));
    }

    return lead;
}


/* UTF-32 */
static inline __attribute__((__pure__)) uint32_t
cng_utf32_utf8_order(uint32_t utf32) {
    if (utf32 < 0x80)
        return 0;
    else if (utf32 < 0x800)
        return 1;
    else if (utf32 < 0x10000)
        return 2;
    else
        return 3;
}


static inline __attribute__((__pure__)) uint32_t
cng_utf32_utf8(uint32_t utf32) {
    uint32_t step;
    uint32_t shift;
    uint32_t utf8 = 0;
    uint32_t const order = cng_utf32_utf8_order(utf32);
    uint32_t const masks = (UINT32_C(0x3F3F3F00) | ((UINT32_C(0x070F1F7F) >> (order << 3)) & 0xFF));
    uint32_t const leads = (UINT32_C(0x80808000) | ((UINT32_C(0xF0E0C000) >> (order << 3)) & 0xFF));

    shift = (order * 8);
    for (step = (order + 1); step != (uint32_t)0; --step) {
        uint32_t const mask = ((masks >> shift) & 0xFF);
        uint32_t const lead = ((leads >> shift) & 0xFF);

        utf8 |= (((utf32 & mask) | lead) << shift);
        utf32 >>= 6;
        shift -= 8;
    }

    return utf8;
}


static inline __attribute__((__pure__)) uint32_t
cng_utf32_utf16(uint32_t utf32) {
    if (utf32 >= UINT32_C(0x10000)) {
        uint32_t const base = (utf32 - UINT32_C(0x10000));
        uint16_t const lead = ((base >> 10) + UINT32_C(0xD800));
        uint16_t const trail = ((base & UINT32_C(0x3FF)) + UINT32_C(0xDC00));

        return (((uint32_t)trail << 16) | (uint32_t)lead);
    }

    return utf32;
}


#ifdef __cplusplus
} /* extern "C" */
#endif
