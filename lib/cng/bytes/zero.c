/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <cng/address.h>

#include <stddef.h>
#include <stdint.h>


void
cng_bytes_zero(void *data, size_t size) {
    unsigned char *bytes = data;

    while (!cng_address_aligned2(bytes, sizeof(uintptr_t))) {
        *bytes = '\0';
        ++bytes;
        --size;
    }

    while (size >= sizeof(uintptr_t)) {
        *(uintptr_t *)bytes = 0;
        bytes += sizeof(uintptr_t);
        size -= sizeof(uintptr_t);
    }

    while (size) {
        *bytes = '\0';
        ++bytes;
        --size;
    }
}
