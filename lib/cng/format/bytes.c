/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/slice.h>

#include <stdarg.h>
#include <stddef.h>


static size_t
cng_format_dump_bytes(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


static size_t
cng_format_load_bytes(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


struct cng_format const cng_format_bytes = {
    cng_format_dump_bytes,
    cng_format_load_bytes,
};


struct cng_format_handler_bytes {
    char const *const tdata;
    size_t const tsize;
    size_t (*const dump)(char const *data, size_t size, unsigned char *bdata, size_t bsize);
    size_t (*const load)(char *data, size_t size, unsigned char const *bdata, size_t bsize);
};


static struct cng_format_handler_bytes const *
cng_format_handler_bytes(char const *tdata, size_t tsize);


static size_t
cng_format_dump_bytes(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    void *bdata = va_arg(ap, void *);
    size_t const bsize = va_arg(ap, size_t);
    struct cng_format_handler_bytes const *handler;

    if ((handler = cng_format_handler_bytes(tdata, tsize)) != NULL)
        return handler->dump(data, size, bdata, bsize);

    return 0;
}


static size_t
cng_format_load_bytes(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    void const *bdata = va_arg(ap, void const *);
    size_t const bsize = va_arg(ap, size_t);
    struct cng_format_handler_bytes const *handler;

    if ((handler = cng_format_handler_bytes(tdata, tsize)) != NULL)
        return handler->load(data, size, bdata, bsize);

    return 0;
}


static size_t
cng_format_dump_bytes_raw(char const *data, size_t size, unsigned char *bdata, size_t bsize) {
    cng_bytes_copy(bdata, data, cng_size_min(bsize, size));

    return size;
}


static size_t
cng_format_load_bytes_raw(char *data, size_t size, unsigned char const *bdata, size_t bsize) {
    cng_bytes_copy(data, bdata, cng_size_min(size, bsize));

    return bsize;
}


static size_t
cng_format_dump_bytes_hex(char const *data, size_t size, unsigned char *bdata, size_t bsize) {
    size_t const limit = cng_size_min((size / 2), (bsize & ~(size_t)1));

    return cng_bytes_unhexlify(bdata, limit, data);
}


static size_t
cng_format_load_bytes_hex(char *data, size_t size, unsigned char const *bdata, size_t bsize) {
    size_t const rv = (bsize * 2);
    size_t const limit = (cng_size_min(rv, size) / 2);

    cng_bytes_hexlify(bdata, limit, data);

    return rv;
}


static struct cng_format_handler_bytes const *
cng_format_handler_bytes(char const *tdata, size_t tsize) {
    struct cng_format_handler_bytes const *handler;
    static struct cng_format_handler_bytes const handlers[] = {
        {
            "raw",
            (sizeof("raw") - 1),
            cng_format_dump_bytes_raw,
            cng_format_load_bytes_raw,
        },
        {
            "hex",
            (sizeof("hex") - 1),
            cng_format_dump_bytes_hex,
            cng_format_load_bytes_hex,
        },
    };

    cng_array_foreach(handler, handlers) {
        if ((handler->tsize == tsize) && (cng_bytes_match(tdata, handler->tdata, tsize) == tsize))
            return handler;
    }

    return NULL;
}
