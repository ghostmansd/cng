/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bitmap.h>

#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>

#include "bitmap.h"


bool
cng_bitmap_test(size_t const *bitmap, size_t bit) {
    const size_t mask = cng_bitmap_mask(bit);
    const atomic_size_t *bucket = cng_bitmap_bucket(bitmap, bit);

    return !!(atomic_load_explicit(bucket, memory_order_acquire) & mask);
}
