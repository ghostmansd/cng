/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bitmap.h>

#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>

#include "bitmap.h"


bool
cng_bitmap_test_and_change(size_t *bitmap, size_t bit) {
    const size_t mask = cng_bitmap_mask(bit);
    atomic_size_t *bucket = cng_bitmap_bucket(bitmap, bit);

    return !!(atomic_fetch_xor_explicit(bucket, mask, memory_order_acq_rel) & mask);
}


void
cng_bitmap_change(size_t *bitmap, size_t bit) {
    const size_t mask = cng_bitmap_mask(bit);
    atomic_size_t *bucket = cng_bitmap_bucket(bitmap, bit);

    (void)atomic_fetch_xor_explicit(bucket, mask, memory_order_release);
}


void
cng_bitmap_change_range(size_t *bitmap, size_t head, size_t tail) {
    size_t iter = head;

    for (; ((iter < tail) && (iter % CNG_BITMAP_BITS)); ++iter)
        cng_bitmap_change(bitmap, iter);

    while (((iter + CNG_BITMAP_BITS) <= tail) && ((iter % CNG_BITMAP_BITS) == 0)) {
        atomic_size_t *bucket = cng_bitmap_bucket(bitmap, iter);

        (void)atomic_fetch_xor_explicit(bucket, ~(size_t)0, memory_order_seq_cst);
        iter += CNG_BITMAP_BITS;
    }

    for (; ((iter < tail) && (iter % CNG_BITMAP_BITS)); ++iter)
        cng_bitmap_change(bitmap, iter);
}
