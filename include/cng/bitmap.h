/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


#define CNG_BITMAP_BITS    (sizeof(size_t) * CHAR_BIT)
#define CNG_BITMAP(BITS) \
    (((BITS) + (CNG_BITMAP_BITS + 1)) / CNG_BITMAP_BITS)


bool
cng_bitmap_test(size_t const *bitmap, size_t bit);


bool
cng_bitmap_test_and_change(size_t *bitmap, size_t bit);


bool
cng_bitmap_test_and_clear(size_t *bitmap, size_t bit);


bool
cng_bitmap_test_and_set(size_t *bitmap, size_t bit);


void
cng_bitmap_change(size_t *bitmap, size_t bit);


void
cng_bitmap_clear(size_t *bitmap, size_t bit);


void
cng_bitmap_set(size_t *bitmap, size_t bit);


void
cng_bitmap_change_range(size_t *bitmap, size_t head, size_t tail);


void
cng_bitmap_clear_range(size_t *bitmap, size_t head, size_t tail);


void
cng_bitmap_set_range(size_t *bitmap, size_t head, size_t tail);


#ifdef __cplusplus
} /* extern "C" */
#endif
