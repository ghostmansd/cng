#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2025 Dmitry Selyutin
# All rights reserved.
#

.section .text


.global cng_ctx_wrap
.type cng_ctx_wrap, STT_FUNC
cng_ctx_wrap:
    callq .Lcng_ctx_get
    movq .Ltrampoline@GOTPCREL(%rip), %r8
    callq cng_ctx_x86_64@PLT
    retq
.Ltrampoline:
    movq %rsi, 0x10(%rsp)
    callq *0x18(%rsp)
    movq 0x10(%rsp), %rdi
    movq %rax, %rsi
    jmp .Lcng_ctx_set


.global cng_ctx_get
.type cng_ctx_get, STT_FUNC
cng_ctx_get:
    leaq 0x08(%rsp), %r9
    movq 0x00(%rsp), %r8
    xorq %rax, %rax
.Lcng_ctx_get:
    movq %r15, 0x38(%rdi)
    movq %r14, 0x30(%rdi)
    movq %r13, 0x28(%rdi)
    movq %r12, 0x20(%rdi)
    movq %rbp, 0x18(%rdi)
    movq %rbx, 0x10(%rdi)
    movq %r9, 0x08(%rdi)
    movq %r8, 0x00(%rdi)
    retq


.global cng_ctx_set
.type cng_ctx_set, STT_FUNC
cng_ctx_set:
    movq %rsi, %rax
.Lcng_ctx_set:
    movq 0x38(%rdi), %r15
    movq 0x30(%rdi), %r14
    movq 0x28(%rdi), %r13
    movq 0x20(%rdi), %r12
    movq 0x18(%rdi), %rbp
    movq 0x10(%rdi), %rbx
    movq 0x08(%rdi), %rsp
    jmpq *0x00(%rdi)


.global cng_ctx_swap
.type cng_ctx_swap, STT_FUNC
cng_ctx_swap:
    movq %rdx, %rax
    leaq 0x08(%rsp), %r9
    movq 0x00(%rsp), %r8
    callq .Lcng_ctx_get

    xchgq %rsi, %rdi
    movq %rdx, %rax
    jmp .Lcng_ctx_set
