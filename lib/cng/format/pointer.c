/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/slice.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>


static size_t
cng_format_dump_pointer(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


static size_t
cng_format_load_pointer(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


struct cng_format const cng_format_pointer = {
    cng_format_dump_pointer,
    cng_format_load_pointer,
};


struct cng_format_handler_pointer {
    char const *const tdata;
    size_t const tsize;
    size_t (*const dump)(char const *data, size_t size, uintptr_t *pointer);
    size_t (*const load)(char *data, size_t size, uintptr_t pointer);
};


static struct cng_format_handler_pointer const *
cng_format_handler_pointer(char const *tdata, size_t tsize);


static size_t
cng_format_dump_pointer(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    uintptr_t *pointer = va_arg(ap, uintptr_t *);
    struct cng_format_handler_pointer const *handler;

    if ((handler = cng_format_handler_pointer(tdata, tsize)))
        return handler->dump(data, size, pointer);

    return 0;
}


static size_t
cng_format_load_pointer(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    uintptr_t const pointer = va_arg(ap, uintptr_t);
    struct cng_format_handler_pointer const *handler;

    if ((handler = cng_format_handler_pointer(tdata, tsize)))
        return handler->load(data, size, pointer);

    return 0;
}


#define CNG_FORMAT_POINTER_BITS \
    (sizeof(uintptr_t) * CHAR_BIT)

#define CNG_FORMAT_POINTER_SHIFT(INDEX) \
    (CNG_FORMAT_POINTER_BITS - CHAR_BIT - (INDEX * CHAR_BIT))


static uintptr_t
cng_format_pointer_fetch(uintptr_t *ref) {
    size_t index;
    uintptr_t value = 0;
    uint8_t *bytes = (uint8_t *)ref;

    for (index = 0; index < sizeof(uintptr_t); ++index)
        value |= ((uintptr_t)bytes[index] << (CNG_FORMAT_POINTER_BITS - 8 - (index * 8)));

    return value;
}


static size_t
cng_format_dump_pointer_hex(char const *data, size_t size, uintptr_t *pointer) {
    size_t rv;
    size_t const limit = cng_size_min((size / 2), sizeof(uintptr_t));

    if ((rv = cng_bytes_unhexlify(pointer, limit, data)) == (sizeof(uintptr_t) * 2))
        *pointer = cng_format_pointer_fetch(pointer);

    return rv;
}


static void
cng_format_pointer_store(uintptr_t *ref, uintptr_t value) {
    size_t index;
    uint8_t *bytes = (uint8_t *)ref;

    for (index = 0; index < sizeof(uintptr_t); ++index)
        bytes[index] = (uint8_t)(value >> (CNG_FORMAT_POINTER_BITS - 8 - (index * 8)));
}


static size_t
cng_format_load_pointer_hex(char *data, size_t size, uintptr_t pointer) {
    size_t const rv = (sizeof(uintptr_t) * 2);
    size_t const limit = (cng_size_min(rv, size) / 2);

    cng_format_pointer_store(&pointer, pointer);
    cng_bytes_hexlify(&pointer, limit, data);

    return rv;
}


static struct cng_format_handler_pointer const *
cng_format_handler_pointer(char const *tdata, size_t tsize) {
    struct cng_format_handler_pointer const *handler;
    static struct cng_format_handler_pointer const handlers[] = {
        {
            "hex",
            (sizeof("hex") - 1),
            cng_format_dump_pointer_hex,
            cng_format_load_pointer_hex,
        },
    };

    cng_array_foreach(handler, handlers) {
        if ((handler->tsize == tsize) && (cng_bytes_match(tdata, handler->tdata, tsize) == tsize))
            return handler;
    }

    return NULL;
}
