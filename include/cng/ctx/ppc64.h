/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


struct cng_ctx_ppc64 {
    /* 0x000 */ uint64_t lr;
    /* 0x008 */ uint64_t r1;
    /* 0x010 */ uint64_t r2;
    /* 0x018 */ uint64_t fpscr;
    /* 0x020 */ uint64_t r14;
    /* 0x028 */ uint64_t r15;
    /* 0x030 */ uint64_t r16;
    /* 0x038 */ uint64_t r17;
    /* 0x040 */ uint64_t r18;
    /* 0x048 */ uint64_t r19;
    /* 0x050 */ uint64_t r20;
    /* 0x058 */ uint64_t r21;
    /* 0x060 */ uint64_t r22;
    /* 0x068 */ uint64_t r23;
    /* 0x070 */ uint64_t r24;
    /* 0x078 */ uint64_t r25;
    /* 0x080 */ uint64_t r26;
    /* 0x088 */ uint64_t r27;
    /* 0x090 */ uint64_t r28;
    /* 0x098 */ uint64_t r29;
    /* 0x0A0 */ uint64_t r30;
    /* 0x0A8 */ uint64_t r31;
    /* 0x0B0 */ uint64_t f14;
    /* 0x0B8 */ uint64_t f15;
    /* 0x0C0 */ uint64_t f16;
    /* 0x0C8 */ uint64_t f17;
    /* 0x0D0 */ uint64_t f18;
    /* 0x0D8 */ uint64_t f19;
    /* 0x0E0 */ uint64_t f20;
    /* 0x0E8 */ uint64_t f21;
    /* 0x0F0 */ uint64_t f22;
    /* 0x0F8 */ uint64_t f23;
    /* 0x100 */ uint64_t f24;
    /* 0x108 */ uint64_t f25;
    /* 0x110 */ uint64_t f26;
    /* 0x118 */ uint64_t f27;
    /* 0x120 */ uint64_t f28;
    /* 0x128 */ uint64_t f29;
    /* 0x130 */ uint64_t f30;
    /* 0x138 */ uint64_t f31;
    /* 0x140 */ uint32_t cr;
    /* 0x144 */ uint32_t : 32;
};


#ifdef __cplusplus
} /* extern "C" */
#endif
