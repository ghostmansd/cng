/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <stddef.h>


int
cng_bytes_compare(void const *ldata, void const *rdata, size_t size) {
    int diff = 0;
    signed char const *lbytes = ldata;
    signed char const *rbytes = rdata;

    while (size--) {
        if ((diff = (*lbytes++ - *rbytes++)) != 0)
            goto complete;
    }

complete:
    return diff;
}
