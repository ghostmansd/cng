/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/slice.h>

#include <limits.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>


_Static_assert(sizeof(uintmax_t) == sizeof(uint64_t),
                   "sizeof(uintmax_t) == sizeof(uint64_t)");
_Static_assert(alignof(uintmax_t) == alignof(uint64_t),
                "alignof(uintmax_t) == alignof(uint64_t)");


static size_t
cng_format_dump_stdint(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


static size_t
cng_format_load_stdint(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap);


struct cng_format const cng_format_stdint = {
    cng_format_dump_stdint,
    cng_format_load_stdint,
};


struct cng_format_stdint_type {
    char const *const tdata;
    size_t const tsize;
    size_t const sign : 1;
    size_t const bits : ((sizeof(size_t) * CHAR_BIT) - 1);
    uintmax_t (*const fetch)(va_list ap, char *sign);
    void (*const store)(va_list ap, uintmax_t value);
    uintmax_t const limit;
};


struct cng_format_alias_stdint {
    char const *const tdata;
    size_t const tsize;
    size_t const sign : 1;
    size_t const bits : ((sizeof(size_t) * CHAR_BIT) - 1);
};


struct cng_format_conv_stdint {
    char const *const tdata;
    size_t const tsize;
    size_t (*const dump)(char const *data, size_t size, uintmax_t *value);
    char const *(*const load)(char *data, uintmax_t value);
};

struct cng_format_handler_stdint {
    struct cng_format_stdint_type const *type;
    struct cng_format_alias_stdint const *alias;
    struct cng_format_conv_stdint const *conv;
};


static bool
cng_format_handler_stdint(char const *tdata, size_t tsize,
        struct cng_format_handler_stdint *handler);


static size_t
cng_format_dump_stdint(char const *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    size_t rv;
    char sign;
    uintmax_t value;
    struct cng_format_handler_stdint handler;

    if (!cng_format_handler_stdint(tdata, tsize, &handler))
        return 0;

    sign = '\0';
    if ((*data == '+') || (*data == '-')) {
        sign = *data;
        ++data;
        --size;
        if (size < 2)
            return 0;
    }
    if (!!sign != !!handler.type->sign)
        return 0;

    if ((rv = handler.conv->dump(data, size, &value)) == size)
        handler.type->store(ap, value);

    return rv;
}


static size_t
cng_format_load_stdint(char *data, size_t size,
        char const *tdata, size_t tsize, va_list ap) {
    size_t rv;
    char sign;
    char storage[64];
    char const *head;
    char const *tail;
    uintmax_t value;
    struct cng_format_handler_stdint handler;

    if (!cng_format_handler_stdint(tdata, tsize, &handler))
        return 0;

    value = handler.type->fetch(ap, &sign);
    if (sign && (size >= 1)) {
        *data = sign;
        ++data;
        --size;
    }

    tail = handler.conv->load(storage, value);
    for (head = storage; head != tail; ++head) {
        if (*head != '0')
            break;
    }
    if (head == tail)
        --head;

    rv = (tail - head);
    cng_bytes_copy(data, head, cng_size_min(rv, size));

    return (!!sign + rv);
}


static struct cng_format_stdint_type const *
cng_format_stdint_type(char const *tdata, size_t tsize);


static struct cng_format_stdint_type const *
cng_format_stdint_typedef(bool sign, size_t bits);


static struct cng_format_alias_stdint const *
cng_format_alias_stdint(char const *tdata, size_t tsize);


static struct cng_format_conv_stdint const *
cng_format_conv_stdint(char const *tdata, size_t tsize);


static bool
cng_format_handler_stdint(char const *tdata, size_t tsize,
        struct cng_format_handler_stdint *handler) {
    struct cng_format_stdint_type const *type;
    struct cng_format_alias_stdint const *alias;
    struct cng_format_conv_stdint const *conv;

    if ((alias = cng_format_alias_stdint(tdata, tsize)) != NULL) {
        if ((type = cng_format_stdint_typedef(alias->sign, alias->bits)) != NULL) {
            tdata += (alias->tsize + 1);
            tsize -= (alias->tsize + 1);
        }
    } else {
        if ((type = cng_format_stdint_type(tdata, tsize)) != NULL) {
            tdata += (type->tsize + 1);
            tsize -= (type->tsize + 1);
        }
    }
    conv = cng_format_conv_stdint(tdata, tsize);

    if (!type || !conv)
        return false;

    handler->type = type;
    handler->alias = alias;
    handler->conv = conv;

    return true;
}


static uintmax_t
cng_format_stdint_schar_fetch(va_list ap, char *sign) {
    signed char const arg = (signed char)va_arg(ap, int);

    if (arg < 0) {
        *sign = '-';
        return -arg;
    } else if (arg > 0) {
        *sign = '+';
        return +arg;
    } else {
        *sign = '\0';
        return arg;
    }
}


static void
cng_format_stdint_schar_store(va_list ap, uintmax_t value) {
    *va_arg(ap, signed char *) = (signed char)value;
}


static uintmax_t
cng_format_stdint_uchar_fetch(va_list ap, char *sign) {
    *sign = '\0';

    return (unsigned char)va_arg(ap, unsigned int);
}


static void
cng_format_stdint_uchar_store(va_list ap, uintmax_t value) {
    *va_arg(ap, unsigned char *) = (unsigned char)value;
}


static uintmax_t
cng_format_stdint_short_fetch(va_list ap, char *sign) {
    short const arg = (short)va_arg(ap, int);

    if (arg < 0) {
        *sign = '-';
        return -arg;
    } else if (arg > 0) {
        *sign = '+';
        return +arg;
    } else {
        *sign = '\0';
        return arg;
    }
}


static void
cng_format_stdint_short_store(va_list ap, uintmax_t value) {
    *va_arg(ap, short *) = (short)value;
}


static uintmax_t
cng_format_stdint_ushort_fetch(va_list ap, char *sign) {
    *sign = '\0';

    return (unsigned short)va_arg(ap, unsigned int);
}


static void
cng_format_stdint_ushort_store(va_list ap, uintmax_t value) {
    *va_arg(ap, unsigned short *) = (unsigned short)value;
}


static uintmax_t
cng_format_stdint_int_fetch(va_list ap, char *sign) {
    int const arg = (int)va_arg(ap, int);

    if (arg < 0) {
        *sign = '-';
        return -arg;
    } else if (arg > 0) {
        *sign = '+';
        return +arg;
    } else {
        *sign = '\0';
        return arg;
    }
}


static void
cng_format_stdint_int_store(va_list ap, uintmax_t value) {
    *va_arg(ap, int *) = (int)value;
}


static uintmax_t
cng_format_stdint_uint_fetch(va_list ap, char *sign) {
    *sign = '\0';

    return (unsigned int)va_arg(ap, unsigned int);
}


static void
cng_format_stdint_uint_store(va_list ap, uintmax_t value) {
    *va_arg(ap, unsigned int *) = (unsigned int)value;
}


static uintmax_t
cng_format_stdint_long_fetch(va_list ap, char *sign) {
    long const arg = (long)va_arg(ap, long);

    if (arg < 0) {
        *sign = '-';
        return -arg;
    } else if (arg > 0) {
        *sign = '+';
        return +arg;
    } else {
        *sign = '\0';
        return arg;
    }
}


static void
cng_format_stdint_long_store(va_list ap, uintmax_t value) {
    *va_arg(ap, long *) = (long)value;
}


static uintmax_t
cng_format_stdint_ulong_fetch(va_list ap, char *sign) {
    *sign = '\0';

    return (unsigned long)va_arg(ap, unsigned long);
}


static void
cng_format_stdint_ulong_store(va_list ap, uintmax_t value) {
    *va_arg(ap, unsigned long *) = (unsigned long)value;
}


static uintmax_t
cng_format_stdint_llong_fetch(va_list ap, char *sign) {
    long long const arg = (long long)va_arg(ap, long long);

    if (arg < 0) {
        *sign = '-';
        return -arg;
    } else if (arg > 0) {
        *sign = '+';
        return +arg;
    } else {
        *sign = '\0';
        return arg;
    }
}


static void
cng_format_stdint_llong_store(va_list ap, uintmax_t value) {
    *va_arg(ap, long long *) = (long long)value;
}


static uintmax_t
cng_format_stdint_ullong_fetch(va_list ap, char *sign) {
    *sign = '\0';

    return (uintmax_t)va_arg(ap, uintmax_t);
}


static void
cng_format_stdint_ullong_store(va_list ap, uintmax_t value) {
    *va_arg(ap, uintmax_t *) = (uintmax_t)value;
}


static struct cng_format_stdint_type const cng_format_stdint_types[] = {
    {
        "schar",
        (sizeof("schar") - 1),
        true,
        (sizeof(signed char) * CHAR_BIT),
        cng_format_stdint_schar_fetch,
        cng_format_stdint_schar_store,
        SCHAR_MAX,
    },
    {
        "uchar",
        (sizeof("uchar") - 1),
        false,
        (sizeof(unsigned char) * CHAR_BIT),
        cng_format_stdint_uchar_fetch,
        cng_format_stdint_uchar_store,
        UCHAR_MAX,
    },
    {
        "short",
        (sizeof("short") - 1),
        true,
        (sizeof(short) * CHAR_BIT),
        cng_format_stdint_short_fetch,
        cng_format_stdint_short_store,
        SHRT_MAX,
    },
    {
        "ushort",
        (sizeof("ushort") - 1),
        false,
        (sizeof(unsigned short) * CHAR_BIT),
        cng_format_stdint_ushort_fetch,
        cng_format_stdint_ushort_store,
        USHRT_MAX,
    },
    {
        "int",
        (sizeof("int") - 1),
        true,
        (sizeof(int) * CHAR_BIT),
        cng_format_stdint_int_fetch,
        cng_format_stdint_int_store,
        INT_MAX,
    },
    {
        "uint",
        (sizeof("uint") - 1),
        false,
        (sizeof(unsigned int) * CHAR_BIT),
        cng_format_stdint_uint_fetch,
        cng_format_stdint_uint_store,
        UINT_MAX,
    },
    {
        "long",
        (sizeof("long") - 1),
        true,
        (sizeof(long) * CHAR_BIT),
        cng_format_stdint_long_fetch,
        cng_format_stdint_long_store,
        LONG_MAX,
    },
    {
        "ulong",
        (sizeof("ulong") - 1),
        false,
        (sizeof(unsigned long) * CHAR_BIT),
        cng_format_stdint_ulong_fetch,
        cng_format_stdint_ulong_store,
        ULONG_MAX,
    },
    {
        "llong",
        (sizeof("llong") - 1),
        true,
        (sizeof(long long) * CHAR_BIT),
        cng_format_stdint_llong_fetch,
        cng_format_stdint_llong_store,
        LLONG_MAX,
    },
    {
        "ullong",
        (sizeof("ullong") - 1),
        false,
        (sizeof(uintmax_t) * CHAR_BIT),
        cng_format_stdint_ullong_fetch,
        cng_format_stdint_ullong_store,
        ULLONG_MAX,
    },
};


static struct cng_format_stdint_type const *
cng_format_stdint_type(char const *tdata, size_t tsize) {
    struct cng_format_stdint_type const *type;

    cng_array_foreach(type, cng_format_stdint_types) {
        if (cng_bytes_match_prefix(tdata, tsize, type->tdata, type->tsize) == type->tsize)
            return ((tdata[type->tsize] == ':') ? type : NULL);
    }

    return NULL;
}


static struct cng_format_stdint_type const *
cng_format_stdint_typedef(bool sign, size_t bits) {
    struct cng_format_stdint_type const *type;

    cng_array_foreach(type, cng_format_stdint_types) {
        if ((sign == type->sign) && (bits == type->bits))
            return type;
    }

    return NULL;
}


static struct cng_format_alias_stdint const *
cng_format_alias_stdint(char const *tdata, size_t tsize) {
    struct cng_format_alias_stdint const *alias;
    static struct cng_format_alias_stdint const aliases[] = {
        {
            "int8",
            (sizeof("int8") - 1),
            true,
            (sizeof(int8_t) * CHAR_BIT),
        },
        {
            "uint8",
            (sizeof("uint8") - 1),
            false,
            (sizeof(uint8_t) * CHAR_BIT),
        },
        {
            "int16",
            (sizeof("int16") - 1),
            true,
            (sizeof(int16_t) * CHAR_BIT),
        },
        {
            "uint16",
            (sizeof("uint16") - 1),
            false,
            (sizeof(uint16_t) * CHAR_BIT),
        },
        {
            "int32",
            (sizeof("int32") - 1),
            true,
            (sizeof(int32_t) * CHAR_BIT),
        },
        {
            "uint32",
            (sizeof("uint32") - 1),
            false,
            (sizeof(uint32_t) * CHAR_BIT),
        },
        {
            "int64",
            (sizeof("int64") - 1),
            true,
            (sizeof(int64_t) * CHAR_BIT),
        },
        {
            "uint64",
            (sizeof("uint64") - 1),
            false,
            (sizeof(uint64_t) * CHAR_BIT),
        },
        {
            "ptrdiff",
            (sizeof("ptrdiff") - 1),
            true,
            (sizeof(ptrdiff_t) * CHAR_BIT),
        },
        {
            "size",
            (sizeof("size") - 1),
            false,
            (sizeof(size_t) * CHAR_BIT),
        },
        {
            "intptr",
            (sizeof("intptr") - 1),
            true,
            (sizeof(intptr_t) * CHAR_BIT),
        },
        {
            "uintptr",
            (sizeof("uintptr") - 1),
            false,
            (sizeof(uintptr_t) * CHAR_BIT),
        },
        {
            "schar",
            (sizeof("schar") - 1),
            true,
            (sizeof(signed char) * CHAR_BIT),
        },
        {
            "uchar",
            (sizeof("uchar") - 1),
            false,
            (sizeof(unsigned char) * CHAR_BIT),
        },
        {
            "short",
            (sizeof("short") - 1),
            true,
            (sizeof(signed short) * CHAR_BIT),
        },
        {
            "ushort",
            (sizeof("ushort") - 1),
            false,
            (sizeof(unsigned short) * CHAR_BIT),
        },
        {
            "int",
            (sizeof("int") - 1),
            true,
            (sizeof(signed int) * CHAR_BIT),
        },
        {
            "uint",
            (sizeof("uint") - 1),
            false,
            (sizeof(unsigned int) * CHAR_BIT),
        },
        {
            "long",
            (sizeof("long") - 1),
            true,
            (sizeof(signed long) * CHAR_BIT),
        },
        {
            "ulong",
            (sizeof("ulong") - 1),
            false,
            (sizeof(unsigned long) * CHAR_BIT),
        },
        {
            "llong",
            (sizeof("llong") - 1),
            true,
            (sizeof(signed long long) * CHAR_BIT),
        },
        {
            "ullong",
            (sizeof("ullong") - 1),
            false,
            (sizeof(uintmax_t) * CHAR_BIT),
        },
    };

    cng_array_foreach(alias, aliases) {
        if (cng_bytes_match_prefix(tdata, tsize, alias->tdata, alias->tsize) == alias->tsize)
            return ((tdata[alias->tsize] == ':') ? alias : NULL);
    }

    return NULL;
}


static size_t
cng_format_dump_stdint_bitwise(char const *data, size_t size,
        unsigned int grade, uintmax_t *value) {
    size_t index;
    uintmax_t number;
    uintmax_t cursor;
    uintmax_t const shift = (grade + 1);
    uintmax_t const base = (1ULL << shift);
    uintmax_t const limit = ((UINT64_C(0x00000D10162040) >> (grade << 3U)) & 0xFFU);

    number = 0;
    cursor = 0;
    size = cng_size_min(size, limit);
    for (index = (size - 1); index != ~(size_t)0; --index) {
        uintmax_t digit;
        unsigned char const byte = (unsigned char)data[index];

        if (('0' <= byte) && (byte <= '9'))
            digit = (byte - (unsigned char)'0');
        else if (('a' <= byte) && (byte <= 'v'))
            digit = (byte - (unsigned char)'a');
        else
            digit = ~(uintmax_t)0;

        if (digit > base)
            return (size - index - 1);

        number |= (digit << cursor);
        cursor += shift;
    }

    *value = number;

    return size;
}


static size_t
cng_format_dump_stdint_2(char const *data, size_t size, uintmax_t *value) {
    return cng_format_dump_stdint_bitwise(data, size, 0, value);
}


static size_t
cng_format_dump_stdint_4(char const *data, size_t size, uintmax_t *value) {
    return cng_format_dump_stdint_bitwise(data, size, 1, value);
}


static size_t
cng_format_dump_stdint_8(char const *data, size_t size, uintmax_t *value) {
    return cng_format_dump_stdint_bitwise(data, size, 2, value);
}


static size_t
cng_format_dump_stdint_10(char const *data, size_t size, uintmax_t *value) {
    size_t index;
    uintmax_t number = 0;
    static uintmax_t const limit = (sizeof("18446744073709551615") - 1);

    for (index = 0; index < size; ++index) {
        unsigned char const digit = data[index];

        if ((digit < '0') || (digit > '9'))
            return index;

        if (index > limit)
            return limit;

        number = ((number * 10U) + ((uintmax_t)digit - (uintmax_t)'0'));
    }

    *value = number;

    return index;
}


static size_t
cng_format_dump_stdint_16(char const *data, size_t size, uintmax_t *value) {
    return cng_format_dump_stdint_bitwise(data, size, 3, value);
}


static size_t
cng_format_dump_stdint_32(char const *data, size_t size, uintmax_t *value) {
    return cng_format_dump_stdint_bitwise(data, size, 4, value);
}


static char const *
cng_format_load_stdint_bitwise(char *storage,
        unsigned int grade, uintmax_t value) {
    size_t index;
    size_t limit;
    uintmax_t const origin = value;
    uintmax_t const shift = (grade + 1);
    uintmax_t const base = (1ULL << shift);
    uintmax_t const mask = (base - 1);
    static char const alphabet[] = "0123456789abcdefghijklmnopqrstuv";

    limit = 1;
    while (value >>= shift)
        ++limit;

    value = origin;
    for (index = 0; index < limit; ++index) {
        storage[limit - index - 1] = alphabet[value & mask];
        value >>= shift;
    }

    return (storage + limit);
}


static char const *
cng_format_load_stdint_2(char *storage, uintmax_t value) {
    return cng_format_load_stdint_bitwise(storage, 0, value);
}


static char const *
cng_format_load_stdint_4(char *storage, uintmax_t value) {
    return cng_format_load_stdint_bitwise(storage, 1, value);
}


static char const *
cng_format_load_stdint_8(char *storage, uintmax_t value) {
    return cng_format_load_stdint_bitwise(storage, 2, value);
}


static char const *
cng_format_load_stdint_16(char *storage, uintmax_t value) {
    return cng_format_load_stdint_bitwise(storage, 3, value);
}


static char const *
cng_format_load_stdint_32(char *storage, uintmax_t value) {
    return cng_format_load_stdint_bitwise(storage, 4, value);
}


extern char const *
cng_format_load_stdint_10(char *storage, uintmax_t value);


static struct cng_format_conv_stdint const *
cng_format_conv_stdint(char const *tdata, size_t tsize) {
    struct cng_format_conv_stdint const *conv;
    static struct cng_format_conv_stdint const convs[] = {
        {
            "2",
            (sizeof("2") - 1),
            cng_format_dump_stdint_2,
            cng_format_load_stdint_2,
        },
        {
            "4",
            (sizeof("4") - 1),
            cng_format_dump_stdint_4,
            cng_format_load_stdint_4,
        },
        {
            "8",
            (sizeof("8") - 1),
            cng_format_dump_stdint_8,
            cng_format_load_stdint_8,
        },
        {
            "10",
            (sizeof("10") - 1),
            cng_format_dump_stdint_10,
            cng_format_load_stdint_10,
        },
        {
            "16",
            (sizeof("16") - 1),
            cng_format_dump_stdint_16,
            cng_format_load_stdint_16,
        },
        {
            "32",
            (sizeof("32") - 1),
            cng_format_dump_stdint_32,
            cng_format_load_stdint_32,
        },
    };

    cng_array_foreach(conv, convs) {
        if ((conv->tsize == tsize) && (cng_bytes_match(tdata, conv->tdata, tsize) == tsize))
            return conv;
    }

    return NULL;
}
