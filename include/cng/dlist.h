/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_dlist {
    struct cng_dlist *head;
    struct cng_dlist *tail;
};
#define CNG_DLIST_INITIALIZER(VAR) \
    { \
        .head = &(VAR), \
        .tail = &(VAR), \
    }


static inline void
cng_dlist_init(struct cng_dlist *link) {
    link->head = link->tail = link;
}


static inline bool
cng_dlist_empty(struct cng_dlist const *link) {
    return ((link->head == link) && (link->tail == link));
}


static inline void *
cng_dlist_entry(struct cng_dlist const *link, size_t offset) {
    if (!link || cng_dlist_empty(link))
        return NULL;
    return (void *)(void const *)((unsigned char const *)link - offset);
}
#define cng_dlist_entry(TYPE, FIELD, LINK) \
    ((TYPE *)cng_dlist_entry((LINK), offsetof(TYPE, FIELD)))


static inline struct cng_dlist *
cng_dlist_back(struct cng_dlist const *root) {
    return (struct cng_dlist *)root->head;
}


#define cng_dlist_back_entry(TYPE, FIELD, ROOT) \
    cng_dlist_entry(TYPE, FIELD, cng_dlist_back(ROOT))


static inline struct cng_dlist *
cng_dlist_front(struct cng_dlist const *root) {
    return (struct cng_dlist *)root->tail;
}


#define cng_dlist_front_entry(TYPE, FIELD, ROOT) \
    cng_dlist_entry(TYPE, FIELD, cng_dlist_front(ROOT))


static inline void
cng_dlist_attach(struct cng_dlist *head, struct cng_dlist *iter, struct cng_dlist *tail) {
    head->tail = iter;
    iter->head = head;
    iter->tail = tail;
    tail->head = iter;
}


static inline void
cng_dlist_push_back(struct cng_dlist *root, struct cng_dlist *link) {
    cng_dlist_attach(cng_dlist_back(root), link, root);
}


static inline void
cng_dlist_push_front(struct cng_dlist *root, struct cng_dlist *link) {
    cng_dlist_attach(root, link, cng_dlist_front(root));
}


static inline struct cng_dlist *
cng_dlist_detach(struct cng_dlist *root) {
    struct cng_dlist *head = root->head;
    struct cng_dlist *tail = root->tail;

    cng_dlist_init(root);
    head->tail = tail;
    tail->head = head;

    return root;
}


static inline struct cng_dlist *
cng_dlist_pop_back(struct cng_dlist *root) {
    return cng_dlist_detach(cng_dlist_back(root));
}


static inline struct cng_dlist *
cng_dlist_pop_front(struct cng_dlist *root) {
    return cng_dlist_detach(cng_dlist_front(root));
}


#define cng_dlist_foreach(VAR, ROOT) \
    for ((VAR) = cng_dlist_back(ROOT); \
         (VAR) != (ROOT); \
         (VAR) = cng_dlist_back(VAR))


#define cng_dlist_foreach_safe(VAR, ROOT, TMP) \
    for ((VAR) = cng_dlist_back(ROOT); \
         ((VAR) != (ROOT)) && (((TMP) = cng_dlist_back(VAR)), 1); \
         (VAR) = (TMP))


#define cng_dlist_foreach_reverse(VAR, ROOT) \
    for ((VAR) = cng_dlist_front(ROOT); \
         (VAR) != (ROOT); \
         (VAR) = cng_dlist_front(VAR))


#define cng_dlist_foreach_safe_reverse(VAR, ROOT, TMP) \
    for ((VAR) = cng_dlist_front(ROOT); \
         ((VAR) != (ROOT)) && (((TMP) = cng_dlist_front(VAR)), 1); \
         (VAR) = (TMP))


#define cng_dlist_foreach_entry(TYPE, FIELD, VAR, ROOT) \
    for ((VAR) = cng_dlist_front_entry(TYPE, FIELD, ROOT); \
         (VAR) && &(VAR)->FIELD != (ROOT); \
         (VAR) = cng_dlist_front_entry(TYPE, FIELD, &(VAR)->FIELD))


#define cng_dlist_foreach_entry_reverse(TYPE, FIELD, VAR, ROOT) \
    for ((VAR) = cng_dlist_back_entry(TYPE, FIELD, ROOT); \
         (VAR) && &(VAR)->FIELD != (ROOT); \
         (VAR) = cng_dlist_back_entry(TYPE, FIELD, &(VAR)->FIELD))


#define cng_dlist_foreach_entry_safe(TYPE, FIELD, VAR, ROOT, TMP) \
    for ((VAR) = cng_dlist_front_entry(TYPE, FIELD, ROOT); \
         (VAR) && \
            ((&(VAR)->FIELD != (ROOT)) && \
            (((TMP) = cng_dlist_front_entry(TYPE, FIELD, &(VAR)->FIELD)), 1)); \
         (VAR) = (TMP))


#define cng_dlist_foreach_entry_safe_reverse(TYPE, FIELD, VAR, ROOT, TMP) \
    for ((VAR) = cng_dlist_back_entry(TYPE, FIELD, ROOT); \
         (VAR) && \
            ((&(VAR)->FIELD != (ROOT)) && \
            (((TMP) = cng_dlist_back_entry(TYPE, FIELD, &(VAR)->FIELD)), 1)); \
         (VAR) = (TMP))


#ifdef __cplusplus
} /* extern "C" */
#endif
