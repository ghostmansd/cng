/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/utf.h>

#include <cng/size.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


ptrdiff_t
cng_utf8_validate(void const *data, size_t size) {
    size_t entry = 0;
    uint8_t const *bytes = data;
    static uint8_t const table[] = {
        /* ASCII */
        0x01, 0x00, 0x7F,

        /* non-overlong 2-byte */
        0x02, 0xC2, 0xDF, 0x80, 0xBF,

        /* excluding overlongs */
        0x03, 0xE0, 0xE0, 0xA0, 0xBF, 0x80, 0xBF,

        /* straight 3-byte */
        0x03, 0xE1, 0xEC, 0x80, 0xBF, 0x80, 0xBF,
        0x03, 0xEE, 0xEE, 0x80, 0xBF, 0x80, 0xBF,
        0x03, 0xEF, 0xEF, 0x80, 0xBF, 0x80, 0xBF,

        /* excluding surrogates */
        0x03, 0xED, 0xED, 0x80, 0x9F, 0x80, 0xBF,

        /* planes 1-3 */
        0x04, 0xF0, 0xF0, 0x90, 0xBF, 0x80, 0xBF, 0x80, 0xBF,

        /* planes 4-15 */
        0x04, 0xF1, 0xF3, 0x80, 0xBF, 0x80, 0xBF, 0x80, 0xBF,

        /* plane 16 */
        0x04, 0xF4, 0xF4, 0x80, 0x8F, 0x80, 0xBF, 0x80, 0xBF,
    };

    while (entry != sizeof(table)) {
        bool ok;
        size_t step;
        size_t const steps = table[entry++];

        if (size < steps)
            goto incomplete;

        ok = true;
        for (step = 0; (ok && (step < steps)); ++step) {
            uint8_t const byte = bytes[step];
            uint8_t const min = table[entry++];
            uint8_t const max = table[entry++];

            if ((byte < min) || (byte > max))
                ok = false;
        }
        if (ok)
            return steps;
    }

incomplete:
    return (size ? -(ptrdiff_t)(cng_utf8_utf32_order(*bytes) + 1) : 0);
}


ptrdiff_t
cng_utf8_fetch(uint32_t *utf8, void const *data, size_t size) {
    ptrdiff_t state;

    if ((state = cng_utf8_validate(data, size)) > 0) {
        uint32_t code = 0;
        uint8_t const *bytes = data;
        size_t index = (size_t)(state - 1);

        for (; index != ~(size_t)0; --index)
            code = ((code << 8) | bytes[index]);

        *utf8 = code;
    }

    return state;
}


ptrdiff_t
cng_utf8_store(uint32_t utf8, void *data, size_t size) {
    ptrdiff_t state;
    uint8_t const sequence[4] = {
        (utf8 >> 0x00),
        (utf8 >> 0x08),
        (utf8 >> 0x10),
        (utf8 >> 0x18),
    };

    if ((state = cng_utf8_validate(sequence, 4)) > 0) {
        size_t index;
        uint8_t *bytes = data;
        size_t const limit = cng_size_min(size, (size_t)state);

        for (index = 0; index < limit; ++index)
            bytes[index] = sequence[index];
    }

    return state;
}
