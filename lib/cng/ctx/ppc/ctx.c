/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/ctx.h>

#include <cng/address.h>
#include <cng/bytes.h>
#include <cng/size.h>

#include <assert.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


extern bool
cng_ctx_ppc(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void));


struct cng_ctx_ppc_stack {
    /* 0x00 */ alignas(16) uint32_t chain;
    /* 0x04 */ alignas(4) uint32_t lr;
    /* 0x08 */ alignas(4) uint32_t callee;
    /* 0x0C */ alignas(4) uint32_t arg;
    /* 0x10 */ alignas(16) uint32_t nil;
    /* 0x14 */ alignas(4) uint32_t caller;
    /* 0x18 */ alignas(4) uint32_t entry;
    /* 0x1C */ alignas(4) uint32_t pad;
};


static_assert(
    sizeof(struct cng_ctx_ppc_stack) == (sizeof(uint32_t) * 8),
    "sizeof(struct cng_ctx_ppc_stack)"
);
static_assert(
    alignof(struct cng_ctx_ppc_stack) == 16,
    "alignof(struct cng_ctx_ppc_stack)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, chain) == (sizeof(uint32_t) * 0),
    "offsetof(struct cng_ctx_ppc_stack, chain)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, lr) == (sizeof(uint32_t) * 1),
    "offsetof(struct cng_ctx_ppc_stack, lr)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, callee) == (sizeof(uint32_t) * 2),
    "offsetof(struct cng_ctx_ppc_stack, callee)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, arg) == (sizeof(uint32_t) * 3),
    "offsetof(struct cng_ctx_ppc_stack, arg)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, nil) == (sizeof(uint32_t) * 4),
    "offsetof(struct cng_ctx_ppc_stack, nil)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, caller) == (sizeof(uint32_t) * 5),
    "offsetof(struct cng_ctx_ppc_stack, caller)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, entry) == (sizeof(uint32_t) * 6),
    "offsetof(struct cng_ctx_ppc_stack, entry)"
);
static_assert(
    offsetof(struct cng_ctx_ppc_stack, pad) == (sizeof(uint32_t) * 7),
    "offsetof(struct cng_ctx_ppc_stack, pad)"
);


static struct cng_ctx_ppc_stack *
cng_ctx_ppc_stack(void *data, size_t size) {
    void const *head = data;
    void const *tail = data;
    struct cng_ctx_ppc_stack *stack;

    if (!cng_address_increase(&tail, size))
        return false;

    stack = cng_address_stack(head, tail,
        sizeof(struct cng_ctx_ppc_stack),
        alignof(struct cng_ctx_ppc_stack));
    if (stack)
        cng_bytes_zero(stack, sizeof(*stack));

    return stack;
}


static void
cng_ctx_ppc_setup(struct cng_ctx *ctx,
        struct cng_ctx_ppc_stack *stack,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_ppc *ppc = &ctx->ppc;

    stack->entry = (uint32_t)(uintptr_t)entry;
    stack->chain = (uint32_t)(uintptr_t)&stack->nil;

    ppc->lr = (uint32_t)(uintptr_t)trampoline;
    ppc->r1 = (uint32_t)(uintptr_t)&stack->chain;
}


bool
cng_ctx_ppc(struct cng_ctx *ctx,
        void *data, size_t size,
        void *(*entry)(struct cng_ctx *ctx, void *arg),
        void (*trampoline)(void)) {
    struct cng_ctx_ppc_stack *stack;

    stack = cng_ctx_ppc_stack(data, size);
    if (!stack)
        return false;

    cng_ctx_ppc_setup(ctx, stack, entry, trampoline);

    return true;
}
