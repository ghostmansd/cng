/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


int
cng_bytes_compare(void const *ldata, void const *rdata, size_t size);


void
cng_bytes_copy(void *ddata, void const *sdata, size_t size);


static inline __attribute__((__pure__)) uint8_t
cng_bytes_fetch8(void const *data) {
    return *(uint8_t const *)data;
}


static inline __attribute__((__pure__)) uint16_t
cng_bytes_fetch16(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;
    union {
        uint8_t bytes[sizeof(uint16_t)];
        uint16_t integer;
    } const value = {{
        bytes[0],
        bytes[1],
    }};

    return value.integer;
}


static inline __attribute__((__const__)) uint16_t
cng_bytes_fetch16le(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint16_t)bytes[0] << UINT16_C(0x00)) |
            ((uint16_t)bytes[1] << UINT16_C(0x08)));
}


static inline __attribute__((__const__)) uint16_t
cng_bytes_fetch16be(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint16_t)bytes[0] << UINT16_C(0x08)) |
            ((uint16_t)bytes[1] << UINT16_C(0x00)));
}


static inline __attribute__((__pure__)) uint32_t
cng_bytes_fetch32(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;
    union {
        uint8_t bytes[sizeof(uint32_t)];
        uint32_t integer;
    } const value = {{
        bytes[0],
        bytes[1],
        bytes[2],
        bytes[3],
    }};

    return value.integer;
}


static inline __attribute__((__const__)) uint32_t
cng_bytes_fetch32le(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint32_t)bytes[0] << UINT32_C(0x00)) |
            ((uint32_t)bytes[1] << UINT32_C(0x08)) |
            ((uint32_t)bytes[2] << UINT32_C(0x10)) |
            ((uint32_t)bytes[3] << UINT32_C(0x18)));
}


static inline __attribute__((__const__)) uint32_t
cng_bytes_fetch32be(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint32_t)bytes[0] << UINT32_C(0x18)) |
            ((uint32_t)bytes[1] << UINT32_C(0x10)) |
            ((uint32_t)bytes[2] << UINT32_C(0x08)) |
            ((uint32_t)bytes[3] << UINT32_C(0x00)));
}


static inline __attribute__((__pure__)) uint64_t
cng_bytes_fetch64(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;
    union {
        uint8_t bytes[sizeof(uint64_t)];
        uint64_t integer;
    } const value = {{
        bytes[0],
        bytes[1],
        bytes[2],
        bytes[3],
        bytes[4],
        bytes[5],
        bytes[6],
        bytes[7],
    }};

    return value.integer;
}


static inline __attribute__((__const__)) uint64_t
cng_bytes_fetch64le(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint64_t)bytes[0] << UINT64_C(0x00)) |
            ((uint64_t)bytes[1] << UINT64_C(0x08)) |
            ((uint64_t)bytes[2] << UINT64_C(0x10)) |
            ((uint64_t)bytes[3] << UINT64_C(0x18)) |
            ((uint64_t)bytes[4] << UINT64_C(0x20)) |
            ((uint64_t)bytes[5] << UINT64_C(0x28)) |
            ((uint64_t)bytes[6] << UINT64_C(0x30)) |
            ((uint64_t)bytes[7] << UINT64_C(0x38)));
}


static inline __attribute__((__const__)) uint64_t
cng_bytes_fetch64be(void const *data) {
    uint8_t const *bytes = (uint8_t const *)data;

    return (((uint64_t)bytes[0] << UINT64_C(0x38)) |
            ((uint64_t)bytes[1] << UINT64_C(0x30)) |
            ((uint64_t)bytes[2] << UINT64_C(0x28)) |
            ((uint64_t)bytes[3] << UINT64_C(0x20)) |
            ((uint64_t)bytes[4] << UINT64_C(0x18)) |
            ((uint64_t)bytes[5] << UINT64_C(0x10)) |
            ((uint64_t)bytes[6] << UINT64_C(0x08)) |
            ((uint64_t)bytes[7] << UINT64_C(0x00)));
}


void
cng_bytes_hexlify(void const *data, size_t size, char *hex);


size_t
cng_bytes_match(void const *ldata, void const *rdata, size_t size);


static inline size_t
cng_bytes_match_prefix(void const *cdata, size_t csize, void const *pdata, size_t psize) {
    return ((psize <= csize) ? cng_bytes_match(cdata, pdata, psize) : 0);
}


static inline size_t
cng_bytes_match_suffix(void const *cdata, size_t csize, void const *pdata, size_t psize) {
    cdata = ((unsigned char const *)cdata + csize - psize);

    return ((psize <= csize) ? cng_bytes_match(cdata, pdata, psize) : 0);
}


size_t
cng_bytes_merge(void *data, size_t size,
        void const *ldata, size_t lsize,
        void const *rdata, size_t rsize);


struct cng_bytes_pattern {
    size_t table[256];
    size_t size;
    void const *data;
};


static inline void
cng_bytes_pattern(struct cng_bytes_pattern *pattern,
        void const *data, size_t size) {
    if (size > 1) {
        size_t index;
        size_t *table = pattern->table;
        unsigned char const *bytes = (unsigned char const *)data;

        if (size <= (sizeof(size_t) * CHAR_BIT)) {
            for (index = 0; index < 256; ++index)
                table[index] = ~(size_t)0;

            for (index = 0; index < size; ++index)
                table[bytes[index]] &= ~((size_t)1 << index);

        } else {
            size_t const limit = (size - 1);

            for (index = 0; index < 256; ++index)
                table[index] = size;

            for (index = 0; index < limit; ++index)
                table[bytes[index]] = (limit - index);
        }
    }

    pattern->data = data;
    pattern->size = size;
}


void *
cng_bytes_search(struct cng_bytes_pattern const *pattern,
        void const *data, size_t size);


static inline void
cng_bytes_store8(void *data, uint8_t value) {
    *(uint8_t *)data = value;
}


static inline void
cng_bytes_store16(void *data, uint16_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = ((uint8_t *)&value)[0];
    bytes[1] = ((uint8_t *)&value)[1];
}


static inline void
cng_bytes_store16le(void *data, uint16_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT16_C(0x00));
    bytes[1] = (uint8_t)(value >> UINT16_C(0x08));
}


static inline void
cng_bytes_store16be(void *data, uint16_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT16_C(0x08));
    bytes[1] = (uint8_t)(value >> UINT16_C(0x00));
}


static inline void
cng_bytes_store32(void *data, uint32_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = ((uint8_t *)&value)[0];
    bytes[1] = ((uint8_t *)&value)[1];
    bytes[2] = ((uint8_t *)&value)[2];
    bytes[3] = ((uint8_t *)&value)[3];
}


static inline void
cng_bytes_store32le(void *data, uint32_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT32_C(0x00));
    bytes[1] = (uint8_t)(value >> UINT32_C(0x08));
    bytes[2] = (uint8_t)(value >> UINT32_C(0x10));
    bytes[3] = (uint8_t)(value >> UINT32_C(0x18));
}


static inline void
cng_bytes_store32be(void *data, uint32_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT32_C(0x18));
    bytes[1] = (uint8_t)(value >> UINT32_C(0x10));
    bytes[2] = (uint8_t)(value >> UINT32_C(0x08));
    bytes[3] = (uint8_t)(value >> UINT32_C(0x00));
}


static inline void
cng_bytes_store64(void *data, uint64_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = ((uint8_t *)&value)[0];
    bytes[1] = ((uint8_t *)&value)[1];
    bytes[2] = ((uint8_t *)&value)[2];
    bytes[3] = ((uint8_t *)&value)[3];
    bytes[4] = ((uint8_t *)&value)[4];
    bytes[5] = ((uint8_t *)&value)[5];
    bytes[6] = ((uint8_t *)&value)[6];
    bytes[7] = ((uint8_t *)&value)[7];
}


static inline void
cng_bytes_store64le(void *data, uint64_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT64_C(0x00));
    bytes[1] = (uint8_t)(value >> UINT64_C(0x08));
    bytes[2] = (uint8_t)(value >> UINT64_C(0x10));
    bytes[3] = (uint8_t)(value >> UINT64_C(0x18));
    bytes[4] = (uint8_t)(value >> UINT64_C(0x20));
    bytes[5] = (uint8_t)(value >> UINT64_C(0x28));
    bytes[6] = (uint8_t)(value >> UINT64_C(0x30));
    bytes[7] = (uint8_t)(value >> UINT64_C(0x38));
}


static inline void
cng_bytes_store64be(void *data, uint64_t value) {
    uint8_t *bytes = (uint8_t *)data;

    bytes[0] = (uint8_t)(value >> UINT64_C(0x38));
    bytes[1] = (uint8_t)(value >> UINT64_C(0x30));
    bytes[2] = (uint8_t)(value >> UINT64_C(0x28));
    bytes[3] = (uint8_t)(value >> UINT64_C(0x20));
    bytes[4] = (uint8_t)(value >> UINT64_C(0x18));
    bytes[5] = (uint8_t)(value >> UINT64_C(0x10));
    bytes[6] = (uint8_t)(value >> UINT64_C(0x08));
    bytes[7] = (uint8_t)(value >> UINT64_C(0x00));
}


size_t
cng_bytes_unhexlify(void *data, size_t size, char const *hex);


void
cng_bytes_wipe(void *data, size_t size);


void
cng_bytes_zero(void *data, size_t size);


#ifdef __cplusplus
} /* extern "C" */
#endif
