/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <stdint.h>


char const *
cng_format_load_stdint_10(char *storage, uintmax_t value) {
    char *rv;
    char *head = storage;
    char *tail = storage;

    do {
        static char const alphabet[] = "0123456789abcdef";

        *tail++ = alphabet[value % 10];
        value /= 10;
    } while (value);

    rv = tail--;
    while (head < tail) {
        char const byte = *head;

        *head = *tail;
        *tail = byte;

        ++head;
        --tail;
    }

    return rv;
}
