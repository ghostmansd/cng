/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


struct cng_ctx_x86_64 {
    uint64_t rip;
    uint64_t rsp;
    uint64_t rbx;
    uint64_t rbp;
    uint64_t r12;
    uint64_t r13;
    uint64_t r14;
    uint64_t r15;
};


#ifdef __cplusplus
} /* extern "C" */
#endif
