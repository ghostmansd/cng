/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_ctx_x86 {
    uint32_t eip;
    uint32_t esp;
    uint32_t ebx;
    uint32_t esi;
    uint32_t edi;
    uint32_t ebp;
};


#ifdef __cplusplus
} /* extern "C" */
#endif
