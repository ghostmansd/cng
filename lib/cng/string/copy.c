/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/utf.h>

#include <stddef.h>


size_t
cng_string_copy(char *target, size_t limit, char const *source) {
    size_t chunk;
    unsigned char byte;
    char const *const origin = source;

    do {
        byte = (unsigned char)*source;
        chunk = (cng_utf8_utf32_order(byte) + 1);
        if (chunk <= limit) {
            limit -= chunk;
            while (chunk--)
                *target++ = *source++;
        } else {
            target += chunk;
            source += chunk;
            limit = 0;
        }
    } while (byte);

    return (source - origin);
}
