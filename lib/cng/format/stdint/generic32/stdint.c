/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/format.h>

#include <cng/bytes.h>
#include <cng/size.h>

#include <stddef.h>
#include <stdint.h>


static inline size_t
cng_format_load_stdint_10_digits(uint32_t value) {
    size_t digits = 0;

    do {
        value /= 10;
        ++digits;
    } while (value);

    return digits;
}


static char const *
cng_format_load_stdint_10_32bit(char *storage, uint32_t value) {
    char *rv;
    char *head = storage;
    char *tail = storage;

    do {
        static char const alphabet[] = "0123456789abcdef";

        *tail++ = alphabet[value % 10];
        value /= 10;
    } while (value);

    rv = tail--;
    while (head < tail) {
        char const byte = *head;

        *head = *tail;
        *tail = byte;

        ++head;
        --tail;
    }

    return rv;
}


static char const *
cng_format_load_stdint_10_64bit(char *storage, uint64_t value) {
    /*
     * Douglas W. Jones, Binary to Decimal Conversion in Limited Precision
     *
     * http://homepage.divms.uiowa.edu/~jones/bcd/decimal.html
     *
     * 2^0  0000,0000,0000,0000,0001
     * 2^16 0000,0000,0000,0006,5536
     * 2^32 0000,0000,0042,9496,7296
     * 2^48 0000,0281,4749,7671,0656
     * 2^64 1844,6744,0737,0955,1616
     */
    size_t index;
    size_t length;
    struct {
        uint32_t chunk;
        uint32_t mul[4];
    } table[] = {
        {((value >> 0x00ULL) & 0xFFFFULL), {   1, 5536, 7296,  656}},
        {((value >> 0x10ULL) & 0xFFFFULL), {   0,    6, 9496, 7671}},
        {((value >> 0x20ULL) & 0xFFFFULL), {   0,    0,   42, 4749}},
        {((value >> 0x30ULL) & 0xFFFFULL), {   0,    0,    0,  281}},
        {                               0, {   0,    0,    0,    0}},
    };
    char repr[] = "00000000000000000000";

    for (size_t i = 0; i < 4; ++i) {
        uint32_t chunk = 0;

        for (size_t j = i; j < 4; ++j)
            chunk += (table[j].chunk * table[i].mul[j]);

        table[i].chunk = (chunk + table[4].chunk);
        table[4].chunk = (table[i].chunk / 10000);
        table[i].chunk %= 10000;
    }

    for (size_t i = 4; i != ~(size_t)0; --i) {
        uint32_t chunk = table[i].chunk;
        size_t const digits = cng_format_load_stdint_10_digits(chunk);
        char *const base = &repr[20 - (i * 4) - digits];

        (void)cng_format_load_stdint_10_32bit(base, chunk);
    }

    for (index = 0; index != (sizeof(repr) - 1); ++index) {
        if (repr[index] != '0')
            break;
    }
    if (index == (sizeof(repr) - 1))
        --index;

    length = ((sizeof(repr) - 1) - index);
    cng_bytes_copy(storage, (repr + index), length);

    return (storage + length);
}


char const *
cng_format_load_stdint_10(char *storage, uintmax_t value) {
    if (value <= (uintmax_t)UINT32_MAX)
        return cng_format_load_stdint_10_32bit(storage, (uint32_t)value);

    return cng_format_load_stdint_10_64bit(storage, (uint64_t)value);
}
