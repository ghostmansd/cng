/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


struct cng_ring {
    unsigned char *storage;
    size_t capacity;
    size_t reader;
    size_t writer;
};
#define CNG_RING_INITIALIZER(STORAGE, CAPACITY) \
    { \
        .storage = (STORAGE), \
        .capacity = (CAPACITY), \
        .reader = 0, \
        .writer = 0, \
    }


static inline void
cng_ring_init(struct cng_ring *ring, void *storage, size_t capacity) {
    ring->storage = (unsigned char *)storage;
    ring->capacity = capacity;
    ring->reader = 0;
    ring->writer = 0;
}


size_t
cng_ring_count(struct cng_ring const *ring);


size_t
cng_ring_space(struct cng_ring const *ring);


static inline bool
cng_ring_empty(struct cng_ring const *ring) {
    return (cng_ring_count(ring) == 0);
}


static inline bool
cng_ring_full(struct cng_ring const *ring) {
    return (cng_ring_space(ring) == 0);
}


size_t
cng_ring_consume(struct cng_ring *ring, void *data, size_t size);


size_t
cng_ring_produce(struct cng_ring *ring, void const *data, size_t size);


#ifdef __cplusplus
} /* extern "C" */
#endif
