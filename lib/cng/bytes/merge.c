/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <stddef.h>


size_t
cng_bytes_merge(void *data, size_t size,
        void const *ldata, size_t lsize,
        void const *rdata, size_t rsize) {
    unsigned char *bytes = data;
    size_t const rv = (lsize + rsize);

    if (lsize <= size) {
        cng_bytes_copy(bytes, ldata, lsize);
        bytes += lsize;
        size -= lsize;
        if (rsize <= size)
            cng_bytes_copy(bytes, rdata, rsize);
    }

    return rv;
}
