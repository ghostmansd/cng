#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2025 Dmitry Selyutin
# All rights reserved.
#

.text
.globl cng_ctx_wrap
.p2align 2
.type cng_ctx_wrap, @function
.section .opd,"aw",@progbits
cng_ctx_wrap:
    .p2align 3
    .quad .L.cng_ctx_wrap
    .quad .TOC.@tocbase
    .quad 0
    .text
.L.cng_ctx_wrap:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    bl .Lcng_ctx_get
    std %r8, 16(%r1)
    stdu %r1, -112(%r1)
    addis %r7, %r2, .Ltrampoline@toc@ha
    addi %r7, %r7, .Ltrampoline@toc@l
    mtlr %r8
    bl cng_ctx_ppc64
    nop
    addi %r1, %r1, 112
    ld %r8, 16(%r1)
    mtlr %r8
    blr
.Ltrampoline:
    mr %r0, %r3
    mr %r3, %r4
    mr %r4, %r0
    mflr %r0
    std %r0, 0x10(%r1)
    std %r10, 0x48(%r1)
    ld %r9, 0x50(%r1)
    std %r2, 0x28(%r1)
    ld %r10, 0x00(%r9)
    ld %r11, 0x10(%r9)
    ld %r2, 0x08(%r9)
    mtctr %r10
    bctrl
    ld %r2, 0x28(%r1)
    mr %r4, %r3
    ld %r3, 0x48(%r1)
    b .Lcng_ctx_set


.text
.globl cng_ctx_get
.p2align 2
.type cng_ctx_get, @function
.section .opd,"aw",@progbits
cng_ctx_get:
    .p2align 3
    .quad .L.cng_ctx_get
    .quad .TOC.@tocbase
    .quad 0
    .text
.L.cng_ctx_get:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    li %r3, 0

.Lcng_ctx_get:
    std %r8, 0x000(%r10)
    std %r1, 0x008(%r10)
    std %r2, 0x010(%r10)
    stfd %f0, 0x018(%r10)
    std %r9, 0x140(%r10)

    std %r14, 0x020(%r10)
    std %r15, 0x028(%r10)
    std %r16, 0x030(%r10)
    std %r17, 0x038(%r10)
    std %r18, 0x040(%r10)
    std %r19, 0x048(%r10)
    std %r20, 0x050(%r10)
    std %r21, 0x058(%r10)
    std %r22, 0x060(%r10)
    std %r23, 0x068(%r10)
    std %r24, 0x070(%r10)
    std %r25, 0x078(%r10)
    std %r26, 0x080(%r10)
    std %r27, 0x088(%r10)
    std %r28, 0x090(%r10)
    std %r29, 0x098(%r10)
    std %r30, 0x0A0(%r10)
    std %r31, 0x0A8(%r10)

    stfd %f14, 0x0B0(%r10)
    stfd %f15, 0x0B8(%r10)
    stfd %f16, 0x0C0(%r10)
    stfd %f17, 0x0C8(%r10)
    stfd %f18, 0x0D0(%r10)
    stfd %f19, 0x0D8(%r10)
    stfd %f20, 0x0E0(%r10)
    stfd %f21, 0x0E8(%r10)
    stfd %f22, 0x0F0(%r10)
    stfd %f23, 0x0F8(%r10)
    stfd %f24, 0x100(%r10)
    stfd %f25, 0x108(%r10)
    stfd %f26, 0x110(%r10)
    stfd %f27, 0x118(%r10)
    stfd %f28, 0x120(%r10)
    stfd %f29, 0x128(%r10)
    stfd %f30, 0x130(%r10)
    stfd %f31, 0x138(%r10)

    blr


.text
.globl cng_ctx_set
.p2align 2
.type cng_ctx_set, @function
.section .opd,"aw",@progbits
cng_ctx_set:
    .p2align 3
    .quad .L.cng_ctx_set
    .quad .TOC.@tocbase
    .quad 0
    .text
.L.cng_ctx_set:
.Lcng_ctx_set:
    mr %r0, %r3
    mr %r3, %r4
    mr %r4, %r0

    ld %r5, 0x000(%r4)
    ld %r1, 0x008(%r4)
    ld %r2, 0x010(%r4)
    lfd %f0, 0x018(%r4)
    ld %r8, 0x140(%r4)

    ld %r14, 0x020(%r4)
    ld %r15, 0x028(%r4)
    ld %r16, 0x030(%r4)
    ld %r17, 0x038(%r4)
    ld %r18, 0x040(%r4)
    ld %r19, 0x048(%r4)
    ld %r20, 0x050(%r4)
    ld %r21, 0x058(%r4)
    ld %r22, 0x060(%r4)
    ld %r23, 0x068(%r4)
    ld %r24, 0x070(%r4)
    ld %r25, 0x078(%r4)
    ld %r26, 0x080(%r4)
    ld %r27, 0x088(%r4)
    ld %r28, 0x090(%r4)
    ld %r29, 0x098(%r4)
    ld %r30, 0x0A0(%r4)
    ld %r31, 0x0A8(%r4)

    lfd %f14, 0x0B0(%r4)
    lfd %f15, 0x0B8(%r4)
    lfd %f16, 0x0C0(%r4)
    lfd %f17, 0x0C8(%r4)
    lfd %f18, 0x0D0(%r4)
    lfd %f19, 0x0D8(%r4)
    lfd %f20, 0x0E0(%r4)
    lfd %f21, 0x0E8(%r4)
    lfd %f22, 0x0F0(%r4)
    lfd %f23, 0x0F8(%r4)
    lfd %f24, 0x100(%r4)
    lfd %f25, 0x108(%r4)
    lfd %f26, 0x110(%r4)
    lfd %f27, 0x118(%r4)
    lfd %f28, 0x120(%r4)
    lfd %f29, 0x128(%r4)
    lfd %f30, 0x130(%r4)
    lfd %f31, 0x138(%r4)

    mtctr %r5
    mtfsf 0xff, %f0
    mtcr %r8

    bctr


.text
.globl cng_ctx_swap
.p2align 2
.type cng_ctx_swap, @function
.section .opd,"aw",@progbits
cng_ctx_swap:
    .p2align 3
    .quad .L.cng_ctx_swap
    .quad .TOC.@tocbase
    .quad 0
    .text
.L.cng_ctx_swap:
    mflr %r8
    mffs %f0
    mfcr %r9
    mr %r10, %r3
    bl .Lcng_ctx_get

    mr %r10, %r3
    mr %r3, %r4
    mr %r4, %r5
    b .Lcng_ctx_set
