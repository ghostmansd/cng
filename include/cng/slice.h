/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


#define cng_slice_size(HEAD, TAIL) ((TAIL) - (HEAD))


#define cng_slice_foreach(ITER, HEAD, TAIL) \
    for ((ITER) = (HEAD); \
         (ITER) != (TAIL); \
         ++(ITER))


#define cng_slice_foreach_reverse(ITER, HEAD, TAIL) \
    for ((ITER) = ((TAIL)-1); \
         (ITER) != ((HEAD)-1); \
         --(ITER))


#define cng_array_head(ARRAY) (ARRAY)
#define cng_array_tail(ARRAY) ((ARRAY) + cng_array_size(ARRAY))
#define cng_array_size(ARRAY) (sizeof(ARRAY) / sizeof(*ARRAY))


#define cng_array_foreach(ITER, ARRAY) \
    cng_slice_foreach(ITER, cng_array_head(ARRAY), cng_array_tail(ARRAY))


#define cng_array_foreach_reverse(ITER, ARRAY) \
    cng_slice_foreach_reverse(ITER, cng_array_head(ARRAY), cng_array_tail(ARRAY))


#ifdef __cplusplus
} /* extern "C" */
#endif
