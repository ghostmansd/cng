/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#pragma once

#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <cng/ctx/ppc.h>
#include <cng/ctx/ppc64.h>
#include <cng/ctx/riscv64.h>
#include <cng/ctx/x86.h>
#include <cng/ctx/x86_64.h>


#ifdef __cplusplus
extern "C" {
#endif


struct cng_ctx;


struct cng_ctx {
    union {
        struct cng_ctx_x86 x86;
        struct cng_ctx_x86_64 x86_64;
        struct cng_ctx_ppc ppc;
        struct cng_ctx_ppc64 ppc64;
        struct cng_ctx_riscv64 riscv64;
    };
};


bool
cng_ctx_wrap(struct cng_ctx *ctx,
        void *stack, size_t length,
        void *(*entry)(struct cng_ctx *ctx, void *arg));


void *
cng_ctx_get(struct cng_ctx *ctx);


void
cng_ctx_set(struct cng_ctx *ctx, void *arg);


void *
cng_ctx_swap(struct cng_ctx *origin, struct cng_ctx *target, void *arg);


#ifdef __cplusplus
} /* extern "C" */
#endif
