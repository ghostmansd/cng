/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/bytes.h>

#include <cng/address.h>
#include <cng/size.h>

#include <stddef.h>


void
cng_bytes_copy(void *ddata, void const *sdata, size_t size) {
    unsigned char *dst = ddata;
    unsigned char const *src = sdata;

    while (size &&
            (!cng_address_aligned2(dst, sizeof(uintptr_t)) ||
             !cng_address_aligned2(src, sizeof(uintptr_t)))) {
        *dst = *src;
        ++dst;
        ++src;
        --size;
    }

    while (size >= sizeof(uintptr_t)) {
        *(uintptr_t *)dst = *(uintptr_t const *)src;
        dst += sizeof(uintptr_t);
        src += sizeof(uintptr_t);
        size -= sizeof(uintptr_t);
    }

    while (size) {
        *dst = *src;
        ++dst;
        ++src;
        --size;
    }
}
