#
# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (c) 2017-2025 Dmitry Selyutin
# All rights reserved.
#

.section .text


.global cng_ctx_wrap
.type cng_ctx_wrap, STT_FUNC
cng_ctx_wrap:
    mv a5, a0
    mv a6, ra
    mv a7, sp
    jal .Lcng_ctx_get
    mv ra, a6
    lla a4, .Ltrampoline
    tail cng_ctx_riscv64@plt
.Ltrampoline:
    mv t0, a0
    mv a0, a1
    mv a1, t0

    sd a5, 0x00(sp)
    ld a6, 0x08(sp)
    sd s0, 0x18(sp)
    jalr a6

    mv a1, a0
    ld a0, 0x00(sp)
    j .Lcng_ctx_set


.global cng_ctx_get
.type cng_ctx_get, STT_FUNC
cng_ctx_get:
    mv a5, a0
    mv a6, ra
    mv a7, sp
    mv a1, a0
    mv a0, zero

.Lcng_ctx_get:
    sd s11, 0x68(a5)
    sd s10, 0x60(a5)
    sd s9, 0x58(a5)
    sd s8, 0x50(a5)
    sd s7, 0x48(a5)
    sd s6, 0x40(a5)
    sd s5, 0x38(a5)
    sd s4, 0x30(a5)
    sd s3, 0x28(a5)
    sd s2, 0x20(a5)
    sd s1, 0x18(a5)
    sd s0, 0x10(a5)
    sd a7, 0x08(a5)
    sd a6, 0x00(a5)
    ret


.global cng_ctx_set
.type cng_ctx_set, STT_FUNC
cng_ctx_set:
.Lcng_ctx_set:
    mv t0, a0
    mv a0, a1
    mv a1, t0

    ld s11, 0x68(a1)
    ld s10, 0x60(a1)
    ld s9, 0x58(a1)
    ld s8, 0x50(a1)
    ld s7, 0x48(a1)
    ld s6, 0x40(a1)
    ld s5, 0x38(a1)
    ld s4, 0x30(a1)
    ld s3, 0x28(a1)
    ld s2, 0x20(a1)
    ld s1, 0x18(a1)
    ld s0, 0x10(a1)
    ld sp, 0x08(a1)
    ld ra, 0x00(a1)
    jr ra


.global cng_ctx_swap
.type cng_ctx_swap, STT_FUNC
cng_ctx_swap:
    mv a5, a0
    mv a6, ra
    mv a7, sp
    jal .Lcng_ctx_get

    mv a0, a1
    mv a1, a2
    j .Lcng_ctx_set
