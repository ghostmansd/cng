/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/utf.h>

#include <stddef.h>
#include <stdint.h>

char const *
cng_string_runes(char const *string, uint32_t *rune) {
    size_t size;
    ptrdiff_t state;
    uint32_t utf8 = 0;

    size = (cng_utf8_utf32_order((unsigned char)*string) + 1);
    if ((state = cng_utf8_fetch(&utf8, string, size)) < 0) {
        *rune = UINT32_C(0xFFFD);
        return NULL;
    }

    *rune = cng_utf8_utf32(utf8);

    return (*rune ? (string + size) : (char const *)NULL);
}
