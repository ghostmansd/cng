/*
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2017-2025 Dmitry Selyutin
 * All rights reserved.
 */

#include <cng/string.h>

#include <cng/bytes.h>
#include <cng/size.h>
#include <cng/utf.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


static inline char const *
cng_string_match_code(char const *string, uint32_t *rv) {
    uint32_t code = 0;
    unsigned char const *bytes = (unsigned char const *)string;
    size_t const order = cng_utf8_utf32_order(*bytes);

    switch (order) {
    case 3:
        code |= ((uint32_t)bytes[3] << 24);
        /* fallthrough */
    case 2:
        code |= ((uint32_t)bytes[2] << 16);
        /* fallthrough */
    case 1:
        code |= ((uint32_t)bytes[1] << 8);
        /* fallthrough */
    default:
        code |= (uint32_t)bytes[0];
    }

    *rv = code;

    return (string + order + 1);
}


bool
cng_string_match(char const *lstring, char const *rstring) {
    uint32_t lcode;
    uint32_t rcode;

    do {
        lstring = cng_string_match_code(lstring, &lcode);
        rstring = cng_string_match_code(rstring, &rcode);
    } while ((lcode == rcode) && (lcode && rcode));

    return (lcode == rcode);
}


bool
cng_string_match_prefix(char const *corpus, char const *pattern) {
    size_t const csize = (cng_string_size(corpus) - 1);
    size_t const psize = (cng_string_size(pattern) - 1);

    return (cng_bytes_match_prefix(corpus, csize, pattern, psize) == psize);
}


bool
cng_string_match_suffix(char const *corpus, char const *pattern) {
    size_t const csize = (cng_string_size(corpus) - 1);
    size_t const psize = (cng_string_size(pattern) - 1);

    return (cng_bytes_match_suffix(corpus, csize, pattern, psize) == psize);
}
